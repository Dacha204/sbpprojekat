﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;
namespace Privilegije_korisnika.Forme
{
    public partial class FormDodelaPrivilegija : Form
    {

        private KorisnikDTO korisnik;
        private RegularnaGrupaDTO grupa;
       
        public FormDodelaPrivilegija(RegularnaGrupaDTO g)
        {
            InitializeComponent();
            grupa = g;
            UcitajPodatke();
            
        }

        public FormDodelaPrivilegija(KorisnikDTO k)
        {
            InitializeComponent();
            korisnik = k;          
            UcitajPodatke();
        }

        private void UcitajPodatke()
        {
            cmbxDodelio.DataSource = ProvajderKorisnik.VratiSveKorisnike(true);
            cmbxDodelio.DisplayMember = "PunoIme";

            chblistPrivilegije.DataSource = ProvajderPrivilegija.SvePrivilegije();
            chblistPrivilegije.DisplayMember = "Naziv";
            List<PrivilegijaDTO> postojece;
            if (korisnik == null)
                postojece = ProvajderPrivilegija.SveGrupaPrivilegije(grupa);
            else
                postojece = ProvajderPrivilegija.SveKorisnikPrivilegije(korisnik);

            for (int i = 0; i < chblistPrivilegije.Items.Count; i++)
            {
                var item = chblistPrivilegije.Items[i] as PrivilegijaDTO;
                if (postojece.Exists(x => x.PrivilegijaId == item.PrivilegijaId))
                    chblistPrivilegije.SetItemChecked(i, true);
            }
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            List<PrivilegijaDTO> privs = chblistPrivilegije.CheckedItems.Cast<PrivilegijaDTO>().ToList();

            if (korisnik != null)
                ProvajderPrivilegija.AzurirajPrivilegijeKorisnika(korisnik,privs,cmbxDodelio.SelectedItem as KorisnikDTO);
            else
                ProvajderPrivilegija.AzurirajPrivilegijeGrupa(grupa, privs, cmbxDodelio.SelectedItem as KorisnikDTO);
            
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
