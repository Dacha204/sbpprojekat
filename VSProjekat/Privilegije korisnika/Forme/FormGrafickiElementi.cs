﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormGrafickiElementiUpravljanje : Form
    {
        public ProfilDTO profil { get; protected set; }
        private List<GuiElementDTO> lista;

        public FormGrafickiElementiUpravljanje(ProfilDTO p)
        {
            InitializeComponent();
            profil = p;
            OsveziListu();
        }

        private void OsveziListu()
        {
            lista = ProvajderKorisnik.VratiGUIElemente();
            dgvGUI.DataSource = lista;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormGrafickiElement fge = new FormGrafickiElement();
            if (fge.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            GuiElementDTO gd = dgvGUI.SelectedRows[0].DataBoundItem as GuiElementDTO;
            FormGrafickiElement fge = new FormGrafickiElement(gd);
            if (fge.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            GuiElementDTO gs = dgvGUI.SelectedRows[0].DataBoundItem as GuiElementDTO;

            if (MessageBox.Show("Da li ste stigurni da zelite da obrisete: " + gs.ElementId, "Brisanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
                DialogResult.Yes) return;

            ProvajderKorisnik.ObrisiGUIElement(gs);
            OsveziListu();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbPretraga_TextChanged(object sender, EventArgs e)
        {
            string criteria = tbPretraga.Text;
            if (criteria != string.Empty)
                dgvGUI.DataSource = lista.FindAll(x =>
                    x.Naziv.ToLower().Contains(criteria));
            else
                dgvGUI.DataSource = lista;
        }
    }
}
