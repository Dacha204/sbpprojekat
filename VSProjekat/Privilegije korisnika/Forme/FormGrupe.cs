﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate.Util;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormGrupe : Form
    {
        public FormGrupe()
        {
            InitializeComponent();
        }
        private void FormGrupe_Shown(object sender, EventArgs e)
        {
            OsveziListu();
            PopuniPomocneGridove();
        }

        private void OsveziListu()
        {
            dgvGrupe.DataSource = Provajderi.ProvajderGrupa.SveRegularneGrupe();
        }

        private void dgvGrupe_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PopuniPomocneGridove();
        }
        private void PopuniPomocneGridove()
        {
            RegularnaGrupaDTO s = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            dgvListaGrupa.DataSource = ProvajderGrupa.RGrupeUGrupi(s);
            dgvListaKorisnika.DataSource = ProvajderGrupa.KGrupeUGrupi(s);
        }
        private void btnDodajGrupuUGrupu_Click(object sender, EventArgs e)
        {
            RegularnaGrupaDTO sel = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            FormDodavanjeUGrupu fg = new FormDodavanjeUGrupu(sel, FormDodavanjeUGrupu.DodajMod.Grupu);
            if (fg.ShowDialog() != DialogResult.OK) return;

            OsveziListu();
            PopuniPomocneGridove();
        }
        private void btnDodajNovuGrupu_Click(object sender, EventArgs e)
        {
            FormGrupa fg = new FormGrupa();
            if (fg.ShowDialog() != DialogResult.OK) return;

            OsveziListu();
            PopuniPomocneGridove(); ;
        }
        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            RegularnaGrupaDTO selected = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            FormGrupa fg = new FormGrupa(selected);
            if (fg.ShowDialog() != DialogResult.OK) return;

            OsveziListu();
            PopuniPomocneGridove();
        }
        private void btnObrisi_Click(object sender, EventArgs e)
        {
            RegularnaGrupaDTO rg = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            
            if (MessageBox.Show("Obrisi " + rg.Ime + "?", "Obrisi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            
            if (!ProvajderGrupa.ObrisiGrupu(rg)) return;

            OsveziListu();
            PopuniPomocneGridove();
        }
        private void btnOsvezi_Click(object sender, EventArgs e)
        {
            OsveziListu();
        }
        private void dgvListaGrupa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void btnObrisiGrupu_Click(object sender, EventArgs e)
        {
            if (dgvListaGrupa.SelectedRows.Count == 0)
                return;

            RegularnaGrupaDTO parrent = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            RegularnaGrupaDTO child = dgvListaGrupa.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;

            bool status = ProvajderGrupa.UkloniGrupuIzGrupe(parrent, child);

            if (!status)
                Utility.PrikaziGresku("Uklanjanje neuspesno");

            OsveziListu();
        }
        private void btnDodajKorisnika_Click(object sender, EventArgs e)
        {
            RegularnaGrupaDTO sel = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            FormDodavanjeUGrupu fg = new FormDodavanjeUGrupu(sel, FormDodavanjeUGrupu.DodajMod.Korisnika);
            if (fg.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            if (dgvListaKorisnika.SelectedRows.Count == 0)
                return;
            RegularnaGrupaDTO parrent = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
            KorisnickaGrupaDTO child = dgvListaKorisnika.SelectedRows[0].DataBoundItem as KorisnickaGrupaDTO;

            bool status = ProvajderGrupa.UkloniKorisnikaIzGrupu(parrent, child);

            if (!status)
                Utility.PrikaziGresku("Uklanjanje neuspesno");
            OsveziListu();
        }
        private void btnZatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDetalji_Click(object sender, EventArgs e)
        {
            if (dgvGrupe.SelectedRows.Count == 0)
                return;

            var sel = dgvGrupe.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;

            FormDodelaPrivilegija fp = new FormDodelaPrivilegija(sel);
            fp.ShowDialog();
        }
    }
}
