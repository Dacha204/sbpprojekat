﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;
using static Privilegije_korisnika.Provajderi.Utility;

namespace Privilegije_korisnika.Forme
{
    public partial class FormGrafickiElement : Form
    {
        private FormMode mode;
        public GuiElementDTO guie { get; protected set; }

        public FormGrafickiElement(GuiElementDTO g)
        {
            InitializeComponent();
            guie = g;
            mode = FormMode.Edit;
            UcitajPodatke();
        }

        public FormGrafickiElement()
        {
            InitializeComponent();
            guie = new GuiElementDTO();
            mode = FormMode.Create;
            UcitajPodatke();
        }

        private void UcitajPodatke()
        {
            tbID.Text = mode == FormMode.Create ? "<novi>" : guie.ElementId.ToString();
            tbNaziv.Text = guie.Naziv;
        }

        
        #region Dogadjaji pritiska na dugme
        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            guie.Naziv = tbNaziv.Text;
            if (mode == FormMode.Create)
                ProvajderKorisnik.KreirajGUIElement(guie);
            else
                ProvajderKorisnik.AzurirajGUIElement(guie);

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnOtkazi_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        #endregion

        #region Validacije unosa
        private bool Validiraj()
        {
            if (this.Controls.Cast<object>().Any(c => c is TextBox && (c as TextBox).Text == String.Empty))
            {
                PraznaPoljaUpozorenje();
                return false;
            }
            return true;
        }
        private void tbNaziv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }
        #endregion
    }
}
