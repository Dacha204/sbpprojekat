﻿namespace Privilegije_korisnika.Forme
{
    partial class FormGrupa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpDatumKreiranja = new System.Windows.Forms.DateTimePicker();
            this.tbOpis = new System.Windows.Forms.TextBox();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.tbId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpLoginOd = new System.Windows.Forms.DateTimePicker();
            this.dtpLoginDo = new System.Windows.Forms.DateTimePicker();
            this.btnOtkazi = new System.Windows.Forms.Button();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.cbxLoginLimit = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxlstIPadrese = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // dtpDatumKreiranja
            // 
            this.dtpDatumKreiranja.Enabled = false;
            this.dtpDatumKreiranja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDatumKreiranja.Location = new System.Drawing.Point(97, 105);
            this.dtpDatumKreiranja.Name = "dtpDatumKreiranja";
            this.dtpDatumKreiranja.Size = new System.Drawing.Size(289, 20);
            this.dtpDatumKreiranja.TabIndex = 40;
            // 
            // tbOpis
            // 
            this.tbOpis.Location = new System.Drawing.Point(97, 79);
            this.tbOpis.Name = "tbOpis";
            this.tbOpis.Size = new System.Drawing.Size(289, 20);
            this.tbOpis.TabIndex = 36;
            this.tbOpis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbOpis_KeyPress);
            // 
            // tbNaziv
            // 
            this.tbNaziv.Location = new System.Drawing.Point(97, 53);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(289, 20);
            this.tbNaziv.TabIndex = 35;
            this.tbNaziv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNaziv_KeyPress);
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(97, 27);
            this.tbId.Name = "tbId";
            this.tbId.ReadOnly = true;
            this.tbId.Size = new System.Drawing.Size(289, 20);
            this.tbId.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Prijava do";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Prijava od";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Datum kreiranja";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Opis";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Naziv";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "ID grupe";
            // 
            // dtpLoginOd
            // 
            this.dtpLoginOd.Enabled = false;
            this.dtpLoginOd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpLoginOd.Location = new System.Drawing.Point(97, 160);
            this.dtpLoginOd.Name = "dtpLoginOd";
            this.dtpLoginOd.Size = new System.Drawing.Size(289, 20);
            this.dtpLoginOd.TabIndex = 41;
            // 
            // dtpLoginDo
            // 
            this.dtpLoginDo.Enabled = false;
            this.dtpLoginDo.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpLoginDo.Location = new System.Drawing.Point(97, 186);
            this.dtpLoginDo.Name = "dtpLoginDo";
            this.dtpLoginDo.Size = new System.Drawing.Size(289, 20);
            this.dtpLoginDo.TabIndex = 42;
            // 
            // btnOtkazi
            // 
            this.btnOtkazi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOtkazi.Location = new System.Drawing.Point(311, 429);
            this.btnOtkazi.Name = "btnOtkazi";
            this.btnOtkazi.Size = new System.Drawing.Size(75, 23);
            this.btnOtkazi.TabIndex = 45;
            this.btnOtkazi.Text = "Otkaži";
            this.btnOtkazi.UseVisualStyleBackColor = true;
            this.btnOtkazi.Click += new System.EventHandler(this.btnOtkazi_Click);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSacuvaj.Location = new System.Drawing.Point(230, 429);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(75, 23);
            this.btnSacuvaj.TabIndex = 44;
            this.btnSacuvaj.Text = "Sačuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // cbxLoginLimit
            // 
            this.cbxLoginLimit.AutoSize = true;
            this.cbxLoginLimit.Location = new System.Drawing.Point(97, 137);
            this.cbxLoginLimit.Name = "cbxLoginLimit";
            this.cbxLoginLimit.Size = new System.Drawing.Size(141, 17);
            this.cbxLoginLimit.TabIndex = 47;
            this.cbxLoginLimit.Text = "Limitiraj vreme logovanja";
            this.cbxLoginLimit.UseVisualStyleBackColor = true;
            this.cbxLoginLimit.CheckedChanged += new System.EventHandler(this.cbxLoginLimit_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "Dozvoljene IP adrese";
            // 
            // cbxlstIPadrese
            // 
            this.cbxlstIPadrese.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxlstIPadrese.FormattingEnabled = true;
            this.cbxlstIPadrese.Location = new System.Drawing.Point(12, 235);
            this.cbxlstIPadrese.Name = "cbxlstIPadrese";
            this.cbxlstIPadrese.Size = new System.Drawing.Size(374, 184);
            this.cbxlstIPadrese.TabIndex = 50;
            // 
            // FormGrupa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 455);
            this.Controls.Add(this.cbxlstIPadrese);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbxLoginLimit);
            this.Controls.Add(this.btnOtkazi);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.dtpLoginDo);
            this.Controls.Add(this.dtpLoginOd);
            this.Controls.Add(this.dtpDatumKreiranja);
            this.Controls.Add(this.tbOpis);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(409, 494);
            this.Name = "FormGrupa";
            this.Text = "Grupa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDatumKreiranja;
        private System.Windows.Forms.TextBox tbOpis;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpLoginOd;
        private System.Windows.Forms.DateTimePicker dtpLoginDo;
        private System.Windows.Forms.Button btnOtkazi;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.CheckBox cbxLoginLimit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckedListBox cbxlstIPadrese;
    }
}