﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika
{
    public partial class FormSifre : Form
    {
        
        public FormSifre(KorisnikDTO kd)
        {
            InitializeComponent();
            LoadGrid(kd.KorisnikId);
        }

        public FormSifre(int korisnikId)
        {
            InitializeComponent();
            LoadGrid(korisnikId);
        }

        private void LoadGrid(int id)
        {
            dgvSifre.DataSource = Provajderi.ProvajderKorisnik.VratiKorisnikoveSifre(id);
        }


    }
}
