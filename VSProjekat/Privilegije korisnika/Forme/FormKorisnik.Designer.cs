﻿namespace Privilegije_korisnika
{
    partial class FormKorisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.tbKorisnickoIme = new System.Windows.Forms.TextBox();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.tbImeRoditelja = new System.Windows.Forms.TextBox();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.tbJmbg = new System.Windows.Forms.TextBox();
            this.tbRadnoMesto = new System.Windows.Forms.TextBox();
            this.tbFunckija = new System.Windows.Forms.TextBox();
            this.tbBrojKancelarije = new System.Windows.Forms.TextBox();
            this.tbTelefon = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbSifra = new System.Windows.Forms.TextBox();
            this.dtpDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.btnProfil = new System.Windows.Forms.Button();
            this.btnOtkazi = new System.Windows.Forms.Button();
            this.btnSifre = new System.Windows.Forms.Button();
            this.btnPrijavljivanja = new System.Windows.Forms.Button();
            this.lstbxKorisnikUGrupe = new System.Windows.Forms.ListBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnIPManager = new System.Windows.Forms.Button();
            this.cbxlistIP = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID korisnika";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Korisničko ime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ime";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ime roditelja";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Prezime";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "JMBG";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Datum rođenja";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Radno mesto";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Funkcija";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 260);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Broj kancelarije";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 286);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Telefon";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(59, 312);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Email";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(95, 23);
            this.tbId.Name = "tbId";
            this.tbId.ReadOnly = true;
            this.tbId.Size = new System.Drawing.Size(440, 20);
            this.tbId.TabIndex = 12;
            // 
            // tbKorisnickoIme
            // 
            this.tbKorisnickoIme.Location = new System.Drawing.Point(95, 49);
            this.tbKorisnickoIme.Name = "tbKorisnickoIme";
            this.tbKorisnickoIme.Size = new System.Drawing.Size(440, 20);
            this.tbKorisnickoIme.TabIndex = 13;
            this.tbKorisnickoIme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKorisnickoIme_KeyPress);
            // 
            // tbIme
            // 
            this.tbIme.Location = new System.Drawing.Point(95, 75);
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(440, 20);
            this.tbIme.TabIndex = 14;
            this.tbIme.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIme_KeyPress);
            // 
            // tbImeRoditelja
            // 
            this.tbImeRoditelja.Location = new System.Drawing.Point(95, 101);
            this.tbImeRoditelja.Name = "tbImeRoditelja";
            this.tbImeRoditelja.Size = new System.Drawing.Size(440, 20);
            this.tbImeRoditelja.TabIndex = 15;
            this.tbImeRoditelja.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbImeRoditelja_KeyPress);
            // 
            // tbPrezime
            // 
            this.tbPrezime.Location = new System.Drawing.Point(95, 127);
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(440, 20);
            this.tbPrezime.TabIndex = 16;
            this.tbPrezime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPrezime_KeyPress);
            // 
            // tbJmbg
            // 
            this.tbJmbg.Location = new System.Drawing.Point(95, 153);
            this.tbJmbg.Name = "tbJmbg";
            this.tbJmbg.Size = new System.Drawing.Size(440, 20);
            this.tbJmbg.TabIndex = 17;
            this.tbJmbg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbJmbg_KeyPress);
            // 
            // tbRadnoMesto
            // 
            this.tbRadnoMesto.Location = new System.Drawing.Point(95, 205);
            this.tbRadnoMesto.Name = "tbRadnoMesto";
            this.tbRadnoMesto.Size = new System.Drawing.Size(440, 20);
            this.tbRadnoMesto.TabIndex = 19;
            this.tbRadnoMesto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRadnoMesto_KeyPress);
            // 
            // tbFunckija
            // 
            this.tbFunckija.Location = new System.Drawing.Point(95, 231);
            this.tbFunckija.Name = "tbFunckija";
            this.tbFunckija.Size = new System.Drawing.Size(440, 20);
            this.tbFunckija.TabIndex = 20;
            this.tbFunckija.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFunckija_KeyPress);
            // 
            // tbBrojKancelarije
            // 
            this.tbBrojKancelarije.Location = new System.Drawing.Point(95, 257);
            this.tbBrojKancelarije.Name = "tbBrojKancelarije";
            this.tbBrojKancelarije.Size = new System.Drawing.Size(440, 20);
            this.tbBrojKancelarije.TabIndex = 21;
            this.tbBrojKancelarije.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBrojKancelarije_KeyPress);
            // 
            // tbTelefon
            // 
            this.tbTelefon.Location = new System.Drawing.Point(95, 283);
            this.tbTelefon.Name = "tbTelefon";
            this.tbTelefon.Size = new System.Drawing.Size(440, 20);
            this.tbTelefon.TabIndex = 22;
            this.tbTelefon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTelefon_KeyPress);
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(95, 309);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(440, 20);
            this.tbEmail.TabIndex = 23;
            this.tbEmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbEmail_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(62, 338);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Šifra";
            // 
            // tbSifra
            // 
            this.tbSifra.Location = new System.Drawing.Point(95, 335);
            this.tbSifra.Name = "tbSifra";
            this.tbSifra.Size = new System.Drawing.Size(440, 20);
            this.tbSifra.TabIndex = 25;
            this.tbSifra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSifra_KeyPress);
            // 
            // dtpDatumRodjenja
            // 
            this.dtpDatumRodjenja.Location = new System.Drawing.Point(95, 179);
            this.dtpDatumRodjenja.Name = "dtpDatumRodjenja";
            this.dtpDatumRodjenja.Size = new System.Drawing.Size(440, 20);
            this.dtpDatumRodjenja.TabIndex = 26;
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Location = new System.Drawing.Point(343, 599);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(93, 33);
            this.btnSacuvaj.TabIndex = 27;
            this.btnSacuvaj.Text = "Sačuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnProfil
            // 
            this.btnProfil.Location = new System.Drawing.Point(142, 562);
            this.btnProfil.Name = "btnProfil";
            this.btnProfil.Size = new System.Drawing.Size(118, 31);
            this.btnProfil.TabIndex = 28;
            this.btnProfil.Text = "Profili";
            this.btnProfil.UseVisualStyleBackColor = true;
            this.btnProfil.Click += new System.EventHandler(this.btnProfil_Click);
            // 
            // btnOtkazi
            // 
            this.btnOtkazi.Location = new System.Drawing.Point(442, 599);
            this.btnOtkazi.Name = "btnOtkazi";
            this.btnOtkazi.Size = new System.Drawing.Size(93, 33);
            this.btnOtkazi.TabIndex = 29;
            this.btnOtkazi.Text = "Otkaži";
            this.btnOtkazi.UseVisualStyleBackColor = true;
            this.btnOtkazi.Click += new System.EventHandler(this.btnOtkazi_Click);
            // 
            // btnSifre
            // 
            this.btnSifre.Location = new System.Drawing.Point(18, 562);
            this.btnSifre.Name = "btnSifre";
            this.btnSifre.Size = new System.Drawing.Size(118, 31);
            this.btnSifre.TabIndex = 30;
            this.btnSifre.Text = "Šifre";
            this.btnSifre.UseVisualStyleBackColor = true;
            this.btnSifre.Click += new System.EventHandler(this.btnSifre_Click);
            // 
            // btnPrijavljivanja
            // 
            this.btnPrijavljivanja.Location = new System.Drawing.Point(266, 562);
            this.btnPrijavljivanja.Name = "btnPrijavljivanja";
            this.btnPrijavljivanja.Size = new System.Drawing.Size(118, 31);
            this.btnPrijavljivanja.TabIndex = 31;
            this.btnPrijavljivanja.Text = "Prijavljivanja";
            this.btnPrijavljivanja.UseVisualStyleBackColor = true;
            this.btnPrijavljivanja.Click += new System.EventHandler(this.btnPrijavljivanja_Click);
            // 
            // lstbxKorisnikUGrupe
            // 
            this.lstbxKorisnikUGrupe.FormattingEnabled = true;
            this.lstbxKorisnikUGrupe.Location = new System.Drawing.Point(18, 383);
            this.lstbxKorisnikUGrupe.Name = "lstbxKorisnikUGrupe";
            this.lstbxKorisnikUGrupe.Size = new System.Drawing.Size(242, 173);
            this.lstbxKorisnikUGrupe.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 367);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Pripada u grupama";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(263, 367);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Dozvoljene IP adrese";
            // 
            // btnIPManager
            // 
            this.btnIPManager.Location = new System.Drawing.Point(442, 562);
            this.btnIPManager.Name = "btnIPManager";
            this.btnIPManager.Size = new System.Drawing.Size(93, 31);
            this.btnIPManager.TabIndex = 36;
            this.btnIPManager.Text = "IP upravljanja...";
            this.btnIPManager.UseVisualStyleBackColor = true;
            this.btnIPManager.Click += new System.EventHandler(this.btnIPManager_Click);
            // 
            // cbxlistIP
            // 
            this.cbxlistIP.CheckOnClick = true;
            this.cbxlistIP.FormattingEnabled = true;
            this.cbxlistIP.Location = new System.Drawing.Point(266, 383);
            this.cbxlistIP.Name = "cbxlistIP";
            this.cbxlistIP.Size = new System.Drawing.Size(269, 169);
            this.cbxlistIP.TabIndex = 37;
            // 
            // FormKorisnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 639);
            this.Controls.Add(this.cbxlistIP);
            this.Controls.Add(this.btnIPManager);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lstbxKorisnikUGrupe);
            this.Controls.Add(this.btnPrijavljivanja);
            this.Controls.Add(this.btnSifre);
            this.Controls.Add(this.btnOtkazi);
            this.Controls.Add(this.btnProfil);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.dtpDatumRodjenja);
            this.Controls.Add(this.tbSifra);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.tbTelefon);
            this.Controls.Add(this.tbBrojKancelarije);
            this.Controls.Add(this.tbFunckija);
            this.Controls.Add(this.tbRadnoMesto);
            this.Controls.Add(this.tbJmbg);
            this.Controls.Add(this.tbPrezime);
            this.Controls.Add(this.tbImeRoditelja);
            this.Controls.Add(this.tbIme);
            this.Controls.Add(this.tbKorisnickoIme);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(600, 1024);
            this.MinimumSize = new System.Drawing.Size(408, 678);
            this.Name = "FormKorisnik";
            this.Text = "Korisnik";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.TextBox tbKorisnickoIme;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.TextBox tbImeRoditelja;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.TextBox tbJmbg;
        private System.Windows.Forms.TextBox tbRadnoMesto;
        private System.Windows.Forms.TextBox tbFunckija;
        private System.Windows.Forms.TextBox tbBrojKancelarije;
        private System.Windows.Forms.TextBox tbTelefon;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbSifra;
        private System.Windows.Forms.DateTimePicker dtpDatumRodjenja;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.Button btnProfil;
        private System.Windows.Forms.Button btnOtkazi;
        private System.Windows.Forms.Button btnSifre;
        private System.Windows.Forms.Button btnPrijavljivanja;
        private System.Windows.Forms.ListBox lstbxKorisnikUGrupe;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnIPManager;
        private System.Windows.Forms.CheckedListBox cbxlistIP;
    }
}