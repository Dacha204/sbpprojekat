﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormUKorisnici : Form
    {

        private List<KorisnikDTO> fullLista;

        public FormUKorisnici()
        {
            InitializeComponent();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OsveziListu()
        {
            Cursor.Current = Cursors.AppStarting;
            fullLista = ProvajderKorisnik.VratiSveKorisnike();
            dgvKorisnici.DataSource = fullLista;
            Cursor.Current = Cursors.Arrow;
        }
        private void UKorisniciForm_Load(object sender, EventArgs e)
        {
        }

        private void btnPrikaziSifre_Click(object sender, EventArgs e)
        {
            if (dgvKorisnici.SelectedRows.Count == 0)
                return;

            FormSifre sf = new FormSifre((KorisnikDTO) dgvKorisnici.SelectedRows[0].DataBoundItem);
            sf.ShowDialog();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string criteria = txtSearch.Text;
            if (criteria != string.Empty)
                dgvKorisnici.DataSource = fullLista.FindAll(x =>
                    x.RadnoMesto.ToLower().Contains(criteria) ||
                    x.KorisnikId.ToString().ToLower().Contains(criteria) ||
                    x.BrojKancelarije.ToLower().Contains(criteria) ||
                    x.Email.ToLower().Contains(criteria) ||
                    x.Funkcija.ToLower().Contains(criteria) ||
                    x.Ime.ToLower().Contains(criteria) ||
                    x.ImeRoditelja.ToLower().Contains(criteria) ||
                    x.Jmbg.ToLower().Contains(criteria) ||
                    x.KorisnickoIme.ToLower().Contains(criteria) ||
                    x.Prezime.ToLower().Contains(criteria) ||
                    x.Telefon.ToLower().Contains(criteria));
            else
                dgvKorisnici.DataSource = fullLista;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormKorisnik fk = new FormKorisnik();
            if (fk.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (dgvKorisnici.SelectedRows.Count == 0)
                return;

            KorisnikDTO k = dgvKorisnici.SelectedRows[0].DataBoundItem as KorisnikDTO;
            FormKorisnik fk = new FormKorisnik(k);
            if (fk.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }

        private void btnOsvezi_Click(object sender, EventArgs e)
        {
            OsveziListu();
        }

        private void btnPrikaziProfile_Click(object sender, EventArgs e)
        {
            KorisnikDTO k = dgvKorisnici.SelectedRows[0].DataBoundItem as KorisnikDTO;
            FormProfili fps = new FormProfili(k.KorisnikId);
            fps.ShowDialog();
        }

        private void btnPrijavljivanja_Click(object sender, EventArgs e)
        {
            KorisnikDTO sk = dgvKorisnici.SelectedRows[0].DataBoundItem as KorisnikDTO;
            
            FormPrijave fp = new FormPrijave(sk);
            fp.ShowDialog();
        }

        private void FormUKorisnici_Shown(object sender, EventArgs e)
        {
            OsveziListu();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            KorisnikDTO ks = dgvKorisnici.SelectedRows[0].DataBoundItem as KorisnikDTO;

            if (MessageBox.Show("Da li ste stigurni da zelite da obrisete korisnika: " + ks.KorisnickoIme, "Brisanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
                DialogResult.Yes) return;

            ProvajderKorisnik.ObrisiKorisnika(ks);
            OsveziListu();
        }

        private void btnPrikaziPrivilegije_Click(object sender, EventArgs e)
        {
            if (dgvKorisnici.SelectedRows.Count == 0)
                return;

            var sel = dgvKorisnici.SelectedRows[0].DataBoundItem as KorisnikDTO;

            FormDodelaPrivilegija fp = new FormDodelaPrivilegija(sel);
            fp.ShowDialog();
        }
    }
}
