﻿namespace Privilegije_korisnika.Forme
{
    partial class FormPrivilegija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.btnOtkazi = new System.Windows.Forms.Button();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.cmbxTip = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxDozvola = new System.Windows.Forms.CheckBox();
            this.cmboxNadMeni = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbMeniPrivilegija = new System.Windows.Forms.GroupBox();
            this.gbInterfejsPrivilegija = new System.Windows.Forms.GroupBox();
            this.gbMeniPrivilegija.SuspendLayout();
            this.gbInterfejsPrivilegija.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Naziv";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(61, 23);
            this.tbId.Name = "tbId";
            this.tbId.ReadOnly = true;
            this.tbId.Size = new System.Drawing.Size(259, 20);
            this.tbId.TabIndex = 4;
            // 
            // tbNaziv
            // 
            this.tbNaziv.Location = new System.Drawing.Point(61, 49);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(259, 20);
            this.tbNaziv.TabIndex = 5;
            // 
            // btnOtkazi
            // 
            this.btnOtkazi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOtkazi.Location = new System.Drawing.Point(245, 236);
            this.btnOtkazi.Name = "btnOtkazi";
            this.btnOtkazi.Size = new System.Drawing.Size(75, 23);
            this.btnOtkazi.TabIndex = 47;
            this.btnOtkazi.Text = "Otkaži";
            this.btnOtkazi.UseVisualStyleBackColor = true;
            this.btnOtkazi.Click += new System.EventHandler(this.btnOtkazi_Click);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSacuvaj.Location = new System.Drawing.Point(164, 236);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(75, 23);
            this.btnSacuvaj.TabIndex = 46;
            this.btnSacuvaj.Text = "Sačuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // cmbxTip
            // 
            this.cmbxTip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxTip.FormattingEnabled = true;
            this.cmbxTip.Items.AddRange(new object[] {
            "Interfejs privilegija",
            "Admin privilegija",
            "Meni privilegija",
            "Funkcionalna privilegija"});
            this.cmbxTip.Location = new System.Drawing.Point(61, 75);
            this.cmbxTip.Name = "cmbxTip";
            this.cmbxTip.Size = new System.Drawing.Size(259, 21);
            this.cmbxTip.TabIndex = 48;
            this.cmbxTip.SelectedIndexChanged += new System.EventHandler(this.cmbxTip_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = "Tip";
            // 
            // cbxDozvola
            // 
            this.cbxDozvola.AutoSize = true;
            this.cbxDozvola.Location = new System.Drawing.Point(6, 19);
            this.cbxDozvola.Name = "cbxDozvola";
            this.cbxDozvola.Size = new System.Drawing.Size(116, 17);
            this.cbxDozvola.TabIndex = 50;
            this.cbxDozvola.Text = "Dozvola koriscenja";
            this.cbxDozvola.UseVisualStyleBackColor = true;
            // 
            // cmboxNadMeni
            // 
            this.cmboxNadMeni.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboxNadMeni.FormattingEnabled = true;
            this.cmboxNadMeni.Location = new System.Drawing.Point(6, 36);
            this.cmboxNadMeni.Name = "cmboxNadMeni";
            this.cmboxNadMeni.Size = new System.Drawing.Size(296, 21);
            this.cmboxNadMeni.TabIndex = 51;
            this.cmboxNadMeni.SelectedIndexChanged += new System.EventHandler(this.cmboxNadMeni_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 52;
            this.label4.Text = "Roditeljski meni";
            // 
            // gbMeniPrivilegija
            // 
            this.gbMeniPrivilegija.Controls.Add(this.cmboxNadMeni);
            this.gbMeniPrivilegija.Controls.Add(this.label4);
            this.gbMeniPrivilegija.Location = new System.Drawing.Point(12, 159);
            this.gbMeniPrivilegija.Name = "gbMeniPrivilegija";
            this.gbMeniPrivilegija.Size = new System.Drawing.Size(308, 67);
            this.gbMeniPrivilegija.TabIndex = 53;
            this.gbMeniPrivilegija.TabStop = false;
            this.gbMeniPrivilegija.Text = "Meni privilegija";
            // 
            // gbInterfejsPrivilegija
            // 
            this.gbInterfejsPrivilegija.Controls.Add(this.cbxDozvola);
            this.gbInterfejsPrivilegija.Location = new System.Drawing.Point(12, 102);
            this.gbInterfejsPrivilegija.Name = "gbInterfejsPrivilegija";
            this.gbInterfejsPrivilegija.Size = new System.Drawing.Size(308, 51);
            this.gbInterfejsPrivilegija.TabIndex = 54;
            this.gbInterfejsPrivilegija.TabStop = false;
            this.gbInterfejsPrivilegija.Text = "Interfejs privilegija";
            // 
            // FormPrivilegija
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 271);
            this.Controls.Add(this.gbInterfejsPrivilegija);
            this.Controls.Add(this.gbMeniPrivilegija);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbxTip);
            this.Controls.Add(this.btnOtkazi);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(348, 245);
            this.Name = "FormPrivilegija";
            this.Text = "Privilegija";
            this.Load += new System.EventHandler(this.FormPrivilegija_Load);
            this.gbMeniPrivilegija.ResumeLayout(false);
            this.gbMeniPrivilegija.PerformLayout();
            this.gbInterfejsPrivilegija.ResumeLayout(false);
            this.gbInterfejsPrivilegija.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.Button btnOtkazi;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.ComboBox cmbxTip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbxDozvola;
        private System.Windows.Forms.ComboBox cmboxNadMeni;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbMeniPrivilegija;
        private System.Windows.Forms.GroupBox gbInterfejsPrivilegija;
    }
}