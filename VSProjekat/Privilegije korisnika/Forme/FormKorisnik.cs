﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Forme;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;
using static Privilegije_korisnika.Provajderi.Utility;
namespace Privilegije_korisnika
{
    public partial class FormKorisnik : Form
    {
        private FormMode formMode;
        public KorisnikDTO korisnik { get; private set; }

        public FormKorisnik()
        {
            InitializeComponent();
            formMode = FormMode.Create;
            korisnik = new KorisnikDTO();
            btnSacuvaj.Text = "Dodaj";
            UcitajPodatke();
            btnSifre.Enabled = false;
            btnProfil.Enabled = false;
            btnPrijavljivanja.Enabled = false;
        }

        public FormKorisnik(KorisnikDTO k)
        {
            InitializeComponent();
            formMode = FormMode.Edit;
            korisnik = k;
            UcitajPodatke();
        }

        private void UcitajPodatke()
        {
            tbId.Text = formMode == FormMode.Create ? "<novi korisnik>" : korisnik.KorisnikId.ToString();
            cbxlistIP.DataSource = ProvajderPrivilegija.VratiSveIP();
            cbxlistIP.DisplayMember = "IpAdresa";

            if (formMode == FormMode.Create) return;
            
            tbKorisnickoIme.Text = korisnik.KorisnickoIme;
            tbIme.Text = korisnik.Ime;
            tbImeRoditelja.Text = korisnik.ImeRoditelja;
            tbPrezime.Text = korisnik.Prezime;
            tbJmbg.Text = korisnik.Jmbg;
            dtpDatumRodjenja.Value = korisnik.DatumRodjenja;
            tbRadnoMesto.Text = korisnik.RadnoMesto;
            tbBrojKancelarije.Text = korisnik.BrojKancelarije;
            tbTelefon.Text = korisnik.Telefon;
            tbEmail.Text = korisnik.Email;
            tbSifra.Text = korisnik.TrenutnaSifra;
            tbFunckija.Text = korisnik.Funkcija;

            lstbxKorisnikUGrupe.DataSource = ProvajderGrupa.VratiGrupeKorisnika(korisnik);
            lstbxKorisnikUGrupe.DisplayMember = "Ime";

            List<IPDTO> dozvoljeneIP = ProvajderPrivilegija.VratiIPKorisnika(korisnik.KorisnikId);
            for (int i = 0; i < cbxlistIP.Items.Count; i++)
            {
                var item = cbxlistIP.Items[i] as IPDTO;
                if (dozvoljeneIP.Exists(x => x.IpAdresa == item.IpAdresa))
                {
                    cbxlistIP.SetItemChecked(i, true);
                }
            }
        }

        private bool Validiraj()
        {
            return Controls.Cast<Control>().All(c => !(c is TextBox) || c.Text != string.Empty);
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (!Validiraj())
            {
                Utility.PraznaPoljaUpozorenje();
                return;
            }

            korisnik.KorisnickoIme = tbKorisnickoIme.Text;
            korisnik.Ime = tbIme.Text;
            korisnik.ImeRoditelja = tbImeRoditelja.Text;
            korisnik.Prezime = tbPrezime.Text;
            korisnik.Jmbg = tbJmbg.Text;
            korisnik.DatumRodjenja = dtpDatumRodjenja.Value;
            korisnik.RadnoMesto = tbRadnoMesto.Text;
            korisnik.BrojKancelarije = tbBrojKancelarije.Text;
            korisnik.Telefon = tbTelefon.Text;
            korisnik.Email = tbEmail.Text;
            korisnik.TrenutnaSifra = tbSifra.Text;
            korisnik.Funkcija = tbFunckija.Text;

            if (formMode == FormMode.Create)
            {
                int idk = ProvajderKorisnik.KreirajKorisnika(korisnik);
                ProvajderPrivilegija.AzurirajIPKorisnika(idk, cbxlistIP.CheckedItems.Cast<IPDTO>().ToList());
            }
            else
            {
                ProvajderKorisnik.AzurirajKorisnika(korisnik);
                ProvajderPrivilegija.AzurirajIPKorisnika(korisnik.KorisnikId,
                    cbxlistIP.CheckedItems.Cast<IPDTO>().ToList());
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnOtkazi_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        #region Validacije unosa
        private void tbKorisnickoIme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbIme_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbImeRoditelja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbPrezime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbJmbg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbRadnoMesto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbFunckija_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbBrojKancelarije_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbTelefon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
        }
            }

        private void tbEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbSifra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        #endregion

        private void btnSifre_Click(object sender, EventArgs e)
        {
            FormSifre fs = new FormSifre(korisnik.KorisnikId);
            fs.ShowDialog();
        }

        private void btnProfil_Click(object sender, EventArgs e)
        {
            FormProfili fp = new FormProfili(korisnik.KorisnikId);
            fp.ShowDialog();
        }

        private void btnPrijavljivanja_Click(object sender, EventArgs e)
        {
            FormPrijave fp = new FormPrijave(korisnik);
            fp.ShowDialog();
        }

        private void btnIPManager_Click(object sender, EventArgs e)
        {
            FormIPUpravljanje fip = new FormIPUpravljanje();
            fip.ShowDialog();
        }
    }
}
