﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormProfili : Form
    {
        private readonly int kid;

        public FormProfili(int kid)
        {
            InitializeComponent();
            this.kid = kid;
            OsveziPodatke();
        }

        private void OsveziPodatke()
        {
            dgvProfili.DataSource = ProvajderKorisnik.VratiKorisnikoveProfile(kid);
        }
        
        #region Dogadjaji pritiska na dugme
        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormProfil fp = new FormProfil(kid);
            if (fp.ShowDialog() == DialogResult.OK)
                OsveziPodatke();
        }
        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            ProfilDTO pd = dgvProfili.SelectedRows[0].DataBoundItem as ProfilDTO;
            FormProfil fp = new FormProfil(pd);
            if (fp.ShowDialog() == DialogResult.OK)
                OsveziPodatke();
        }
        private void btnObrisi_Click(object sender, EventArgs e)
        {
            ProfilDTO ks = dgvProfili.SelectedRows[0].DataBoundItem as ProfilDTO;

            if (MessageBox.Show("Da li ste stigurni da zelite da obrisete korisnika: " + ks.RedniBroj, "Brisanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
                DialogResult.Yes) return;

            ProvajderKorisnik.ObrisiProfil(ks);
            OsveziPodatke();
        }
        private void btnPrikaziSifre_Click(object sender, EventArgs e)
        {
            ProfilDTO pd = dgvProfili.SelectedRows[0].DataBoundItem as ProfilDTO;
            FormProfilPregled fgl = new FormProfilPregled(pd);
            fgl.ShowDialog();
        }
        private void btnZatvori_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        private void btnOsvezi_Click(object sender, EventArgs e)
        {
            OsveziPodatke();
        }
    }
}
