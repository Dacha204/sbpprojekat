﻿namespace Privilegije_korisnika.Forme
{
    partial class FormGrupe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.dgvGrupe = new System.Windows.Forms.DataGridView();
            this.btnOsvezi = new System.Windows.Forms.Button();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.dgvListaGrupa = new System.Windows.Forms.DataGridView();
            this.dgvListaKorisnika = new System.Windows.Forms.DataGridView();
            this.btnDodajGrupu = new System.Windows.Forms.Button();
            this.btnObrisiGrupu = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDodajKorisnika = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPrivilegije = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaGrupa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaKorisnika)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(9, 105);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(96, 33);
            this.btnObrisi.TabIndex = 6;
            this.btnObrisi.Text = "Obriši grupu";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(9, 66);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(96, 33);
            this.btnIzmeni.TabIndex = 5;
            this.btnIzmeni.Text = "Izmeni grupu";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(9, 27);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(96, 33);
            this.btnDodaj.TabIndex = 4;
            this.btnDodaj.Text = "Dodaj grupu";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodajNovuGrupu_Click);
            // 
            // dgvGrupe
            // 
            this.dgvGrupe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.dgvGrupe, 2);
            this.dgvGrupe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvGrupe.Location = new System.Drawing.Point(3, 20);
            this.dgvGrupe.MultiSelect = false;
            this.dgvGrupe.Name = "dgvGrupe";
            this.dgvGrupe.ReadOnly = true;
            this.dgvGrupe.RowHeadersVisible = false;
            this.dgvGrupe.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvGrupe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGrupe.Size = new System.Drawing.Size(1058, 273);
            this.dgvGrupe.TabIndex = 7;
            this.dgvGrupe.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGrupe_CellClick);
            // 
            // btnOsvezi
            // 
            this.btnOsvezi.Location = new System.Drawing.Point(9, 183);
            this.btnOsvezi.Name = "btnOsvezi";
            this.btnOsvezi.Size = new System.Drawing.Size(96, 34);
            this.btnOsvezi.TabIndex = 12;
            this.btnOsvezi.Text = "Osvezi listu";
            this.btnOsvezi.UseVisualStyleBackColor = true;
            this.btnOsvezi.Click += new System.EventHandler(this.btnOsvezi_Click);
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnZatvori.Location = new System.Drawing.Point(9, 586);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(96, 33);
            this.btnZatvori.TabIndex = 11;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // dgvListaGrupa
            // 
            this.dgvListaGrupa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaGrupa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaGrupa.Location = new System.Drawing.Point(3, 318);
            this.dgvListaGrupa.MultiSelect = false;
            this.dgvListaGrupa.Name = "dgvListaGrupa";
            this.dgvListaGrupa.ReadOnly = true;
            this.dgvListaGrupa.RowHeadersVisible = false;
            this.dgvListaGrupa.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvListaGrupa.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListaGrupa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListaGrupa.Size = new System.Drawing.Size(520, 247);
            this.dgvListaGrupa.TabIndex = 13;
            this.dgvListaGrupa.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListaGrupa_CellContentClick);
            // 
            // dgvListaKorisnika
            // 
            this.dgvListaKorisnika.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaKorisnika.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaKorisnika.Location = new System.Drawing.Point(529, 318);
            this.dgvListaKorisnika.MultiSelect = false;
            this.dgvListaKorisnika.Name = "dgvListaKorisnika";
            this.dgvListaKorisnika.ReadOnly = true;
            this.dgvListaKorisnika.RowHeadersVisible = false;
            this.dgvListaKorisnika.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvListaKorisnika.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvListaKorisnika.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListaKorisnika.Size = new System.Drawing.Size(532, 247);
            this.dgvListaKorisnika.TabIndex = 14;
            // 
            // btnDodajGrupu
            // 
            this.btnDodajGrupu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDodajGrupu.Location = new System.Drawing.Point(3, 3);
            this.btnDodajGrupu.Name = "btnDodajGrupu";
            this.btnDodajGrupu.Size = new System.Drawing.Size(96, 33);
            this.btnDodajGrupu.TabIndex = 15;
            this.btnDodajGrupu.Text = "Dodaj grupu";
            this.btnDodajGrupu.UseVisualStyleBackColor = true;
            this.btnDodajGrupu.Click += new System.EventHandler(this.btnDodajGrupuUGrupu_Click);
            // 
            // btnObrisiGrupu
            // 
            this.btnObrisiGrupu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnObrisiGrupu.Location = new System.Drawing.Point(105, 3);
            this.btnObrisiGrupu.Name = "btnObrisiGrupu";
            this.btnObrisiGrupu.Size = new System.Drawing.Size(96, 33);
            this.btnObrisiGrupu.TabIndex = 16;
            this.btnObrisiGrupu.Text = "Ukloni grupu";
            this.btnObrisiGrupu.UseVisualStyleBackColor = true;
            this.btnObrisiGrupu.Click += new System.EventHandler(this.btnObrisiGrupu_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(105, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 33);
            this.button1.TabIndex = 18;
            this.button1.Text = "Ukloni korisnika";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnDodajKorisnika
            // 
            this.btnDodajKorisnika.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDodajKorisnika.Location = new System.Drawing.Point(3, 3);
            this.btnDodajKorisnika.Name = "btnDodajKorisnika";
            this.btnDodajKorisnika.Size = new System.Drawing.Size(96, 33);
            this.btnDodajKorisnika.TabIndex = 17;
            this.btnDodajKorisnika.Text = "Dodaj korisnika";
            this.btnDodajKorisnika.UseVisualStyleBackColor = true;
            this.btnDodajKorisnika.Click += new System.EventHandler(this.btnDodajKorisnika_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Grupe";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Grupe koje grupa sadrzi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(529, 296);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Korisnici u grupi";
            // 
            // btnPrivilegije
            // 
            this.btnPrivilegije.Location = new System.Drawing.Point(9, 144);
            this.btnPrivilegije.Name = "btnPrivilegije";
            this.btnPrivilegije.Size = new System.Drawing.Size(96, 33);
            this.btnPrivilegije.TabIndex = 23;
            this.btnPrivilegije.Text = "Privilegije grupe";
            this.btnPrivilegije.UseVisualStyleBackColor = true;
            this.btnPrivilegije.Click += new System.EventHandler(this.btnDetalji_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.52107F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.47893F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvGrupe, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dgvListaGrupa, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dgvListaKorisnika, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(111, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1064, 614);
            this.tableLayoutPanel1.TabIndex = 24;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnDodajGrupu);
            this.flowLayoutPanel1.Controls.Add(this.btnObrisiGrupu);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 571);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(520, 40);
            this.flowLayoutPanel1.TabIndex = 25;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnDodajKorisnika);
            this.flowLayoutPanel2.Controls.Add(this.button1);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(529, 571);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(532, 40);
            this.flowLayoutPanel2.TabIndex = 26;
            // 
            // FormGrupe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 638);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnPrivilegije);
            this.Controls.Add(this.btnOsvezi);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.btnIzmeni);
            this.Controls.Add(this.btnDodaj);
            this.MinimumSize = new System.Drawing.Size(1093, 649);
            this.Name = "FormGrupe";
            this.Text = "Grupe";
            this.Shown += new System.EventHandler(this.FormGrupe_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaGrupa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaKorisnika)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.DataGridView dgvGrupe;
        private System.Windows.Forms.Button btnOsvezi;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.DataGridView dgvListaGrupa;
        private System.Windows.Forms.DataGridView dgvListaKorisnika;
        private System.Windows.Forms.Button btnDodajGrupu;
        private System.Windows.Forms.Button btnObrisiGrupu;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnDodajKorisnika;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPrivilegije;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
    }
}