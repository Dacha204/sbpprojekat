﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormPrivilegije : Form
    {

        public FormPrivilegije()
        {
            InitializeComponent();
        }

        #region Dogadjaji pritiska na dugme
        private void btnDodaj_Click(object sender, EventArgs e)
        {
            FormPrivilegija fk = new FormPrivilegija();
            if (fk.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (dgvPrivilegija.SelectedRows.Count == 0)
                return;

            PrivilegijaDTO k = dgvPrivilegija.SelectedRows[0].DataBoundItem as PrivilegijaDTO;
            FormPrivilegija fk = new FormPrivilegija(k);
            if (fk.ShowDialog() == DialogResult.OK)
                OsveziListu();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (dgvPrivilegija.SelectedRows.Count == 0)
                return;
            var a = dgvPrivilegija.SelectedRows[0].DataBoundItem;
            DialogResult dr = MessageBox.Show($"Da li ste sigurni da zelite da obrisete {(a as PrivilegijaDTO).Naziv}?",
                "Brisanje privilegije",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (dr == DialogResult.No)
                return;
            
            if (a is AdminPrivilegijaDTO)
                ProvajderPrivilegija.ObrisiAdminPrivilegiju(a as AdminPrivilegijaDTO);
            else if (a is MeniPrivilegijaDTO)
                ProvajderPrivilegija.ObrisiMeniPrivilegiju(a as MeniPrivilegijaDTO);
            else if (a is InterfejsPrivilegijaDTO)
                ProvajderPrivilegija.ObrisiInterfejsPrivilegiju(a as InterfejsPrivilegijaDTO);
            else if (a is FunckionalnaPrivilegijaDTO)
                ProvajderPrivilegija.ObrisiFunckionalnuPrivilegiju(a as FunckionalnaPrivilegijaDTO);
           
            OsveziListu();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Validacije unosa
        private void tbPretraga_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar))
                e.Handled = true;
        }
        #endregion

        private void FormPrivilegije_Load(object sender, EventArgs e)
        {
            cmbxTipSelektor.SelectedIndex = 0;
        }

        private void OsveziListu()
        {
            switch (cmbxTipSelektor.SelectedIndex)
            {
               case 1:
                    dgvPrivilegija.DataSource = ProvajderPrivilegija.SvePrivilegije()
                        .FindAll(x => x is InterfejsPrivilegijaDTO);
                    break;
                case 2:
                    dgvPrivilegija.DataSource = ProvajderPrivilegija.SvePrivilegije()
                        .FindAll(x => x is AdminPrivilegijaDTO);
                    break;
                case 3:
                    dgvPrivilegija.DataSource = ProvajderPrivilegija.SvePrivilegije()
                        .FindAll(x => x is MeniPrivilegijaDTO);
                    break;
                case 4:
                    dgvPrivilegija.DataSource = ProvajderPrivilegija.SvePrivilegije()
                        .FindAll(x => x is FunckionalnaPrivilegijaDTO);
                    break;

                default:
                    dgvPrivilegija.DataSource = ProvajderPrivilegija.SvePrivilegije();
                    break;
            }
            
        }
        private void cmbxTipSelektor_SelectedIndexChanged(object sender, EventArgs e)
        {
            OsveziListu();
        }
    }
}
