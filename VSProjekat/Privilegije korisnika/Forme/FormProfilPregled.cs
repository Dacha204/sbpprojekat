﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormProfilPregled : Form
    {
        private ProfilDTO profil;

        public FormProfilPregled(ProfilDTO pd)
        {
            InitializeComponent();
            profil = pd;
            UcitajPodatke();
        }

        private void UcitajPodatke()
        {
            txtID.Text = profil.RedniBroj.ToString();
            txtNaziv.Text = profil.BojaPozadine;
            dgvProfilGuiElementi.DataSource = ProvajderKorisnik.VratiGUIElementeProfila(profil);
        }
    }
}
