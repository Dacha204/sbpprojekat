﻿namespace Privilegije_korisnika.Forme
{
    partial class FormDodelaPrivilegija
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupbox = new System.Windows.Forms.GroupBox();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.chblistPrivilegije = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbxDodelio = new System.Windows.Forms.ComboBox();
            this.groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupbox
            // 
            this.groupbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupbox.Controls.Add(this.chblistPrivilegije);
            this.groupbox.Location = new System.Drawing.Point(12, 37);
            this.groupbox.Name = "groupbox";
            this.groupbox.Size = new System.Drawing.Size(547, 343);
            this.groupbox.TabIndex = 1;
            this.groupbox.TabStop = false;
            this.groupbox.Text = "<Naziv Grupe/Korisnika>";
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZatvori.Location = new System.Drawing.Point(484, 386);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(75, 23);
            this.btnZatvori.TabIndex = 2;
            this.btnZatvori.Text = "Otkazi";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSacuvaj.Location = new System.Drawing.Point(403, 386);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(75, 23);
            this.btnSacuvaj.TabIndex = 3;
            this.btnSacuvaj.Text = "Sacuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // chblistPrivilegije
            // 
            this.chblistPrivilegije.CheckOnClick = true;
            this.chblistPrivilegije.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chblistPrivilegije.FormattingEnabled = true;
            this.chblistPrivilegije.Location = new System.Drawing.Point(3, 16);
            this.chblistPrivilegije.Name = "chblistPrivilegije";
            this.chblistPrivilegije.Size = new System.Drawing.Size(541, 324);
            this.chblistPrivilegije.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Dodelio";
            // 
            // cmbxDodelio
            // 
            this.cmbxDodelio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxDodelio.FormattingEnabled = true;
            this.cmbxDodelio.Location = new System.Drawing.Point(61, 6);
            this.cmbxDodelio.Name = "cmbxDodelio";
            this.cmbxDodelio.Size = new System.Drawing.Size(495, 21);
            this.cmbxDodelio.TabIndex = 5;
            // 
            // FormDodelaPrivilegija
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 421);
            this.Controls.Add(this.cmbxDodelio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.groupbox);
            this.Name = "FormDodelaPrivilegija";
            this.Text = "Dodela privilegija";
            this.groupbox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupbox;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.CheckedListBox chblistPrivilegije;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbxDodelio;
    }
}