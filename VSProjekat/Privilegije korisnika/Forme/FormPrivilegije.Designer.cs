﻿namespace Privilegije_korisnika.Forme
{
    partial class FormPrivilegije
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnZatvori = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.dgvPrivilegija = new System.Windows.Forms.DataGridView();
            this.cmbxTipSelektor = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrivilegija)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbxTipSelektor);
            this.panel1.Controls.Add(this.btnZatvori);
            this.panel1.Controls.Add(this.btnObrisi);
            this.panel1.Controls.Add(this.btnIzmeni);
            this.panel1.Controls.Add(this.btnDodaj);
            this.panel1.Controls.Add(this.dgvPrivilegija);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 363);
            this.panel1.TabIndex = 10;
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnZatvori.Location = new System.Drawing.Point(3, 327);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(134, 33);
            this.btnZatvori.TabIndex = 8;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(3, 107);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(134, 33);
            this.btnObrisi.TabIndex = 3;
            this.btnObrisi.Text = "Obriši privilegiju";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(3, 68);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(134, 33);
            this.btnIzmeni.TabIndex = 2;
            this.btnIzmeni.Text = "Izmeni privilegiju";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(3, 29);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(134, 33);
            this.btnDodaj.TabIndex = 1;
            this.btnDodaj.Text = "Dodaj privilegiju";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // dgvPrivilegija
            // 
            this.dgvPrivilegija.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPrivilegija.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrivilegija.Location = new System.Drawing.Point(143, 29);
            this.dgvPrivilegija.MultiSelect = false;
            this.dgvPrivilegija.Name = "dgvPrivilegija";
            this.dgvPrivilegija.ReadOnly = true;
            this.dgvPrivilegija.RowHeadersVisible = false;
            this.dgvPrivilegija.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPrivilegija.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPrivilegija.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPrivilegija.Size = new System.Drawing.Size(435, 331);
            this.dgvPrivilegija.TabIndex = 0;
            // 
            // cmbxTipSelektor
            // 
            this.cmbxTipSelektor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxTipSelektor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxTipSelektor.FormattingEnabled = true;
            this.cmbxTipSelektor.Items.AddRange(new object[] {
            "Sve privilegije",
            "Interfejs privilegije",
            "Admin privilegije",
            "Meni privilegije",
            "Funkcionalne privilegije"});
            this.cmbxTipSelektor.Location = new System.Drawing.Point(354, 3);
            this.cmbxTipSelektor.Name = "cmbxTipSelektor";
            this.cmbxTipSelektor.Size = new System.Drawing.Size(224, 21);
            this.cmbxTipSelektor.TabIndex = 9;
            this.cmbxTipSelektor.SelectedIndexChanged += new System.EventHandler(this.cmbxTipSelektor_SelectedIndexChanged);
            // 
            // FormPrivilegije
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 363);
            this.Controls.Add(this.panel1);
            this.Name = "FormPrivilegije";
            this.Text = "Privilegije";
            this.Load += new System.EventHandler(this.FormPrivilegije_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrivilegija)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.DataGridView dgvPrivilegija;
        private System.Windows.Forms.ComboBox cmbxTipSelektor;
    }
}