﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;

namespace Privilegije_korisnika.Forme
{
    public partial class FormPrijava : Form
    {
        public FormPrijava()
        {
            InitializeComponent();
        }

        private bool Validiraj()
        {
            if (this.Controls.Cast<object>().Any(c => c is TextBox && (c as TextBox).Text == String.Empty))
            {
                Utility.PraznaPoljaUpozorenje();
                return false;
            }
            return true;
        }
        #region Dogadjaji pritiska na dugme
        private void btnPrijava_Click(object sender, EventArgs e)
        {
            if (!Validiraj())
                return;

            bool uspena = ProvajderKorisnik.PrijaviKorisnika(tbKorisnickoEmail.Text, tbSifra.Text, tbIP.Text);

            if (uspena)
            {
                MessageBox.Show("Uspesna prijava");
                DialogResult = DialogResult.OK;
                
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Validacije unosa
        private void tbKorisnickoEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbSifra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) &&
                e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '.' && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        #endregion
    }
}
