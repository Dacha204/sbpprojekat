﻿namespace Privilegije_korisnika
{
    partial class FormSifre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvSifre = new System.Windows.Forms.DataGridView();
            this.btnZatvori = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSifre)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSifre
            // 
            this.dgvSifre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSifre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSifre.Location = new System.Drawing.Point(0, 1);
            this.dgvSifre.MultiSelect = false;
            this.dgvSifre.Name = "dgvSifre";
            this.dgvSifre.ReadOnly = true;
            this.dgvSifre.RowHeadersVisible = false;
            this.dgvSifre.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSifre.Size = new System.Drawing.Size(659, 395);
            this.dgvSifre.TabIndex = 0;
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZatvori.Location = new System.Drawing.Point(584, 402);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(75, 23);
            this.btnZatvori.TabIndex = 1;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            // 
            // FormSifre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 427);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.dgvSifre);
            this.Name = "FormSifre";
            this.Text = "Šifre korisnika";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSifre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvSifre;
        private System.Windows.Forms.Button btnZatvori;
    }
}