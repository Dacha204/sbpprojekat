﻿namespace Privilegije_korisnika.Forme
{
    partial class FormUKorisnici
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvKorisnici = new System.Windows.Forms.DataGridView();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnPrikaziSifre = new System.Windows.Forms.Button();
            this.btnPrikaziProfile = new System.Windows.Forms.Button();
            this.btnPrikaziPrivilegije = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPretraga = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnOsvezi = new System.Windows.Forms.Button();
            this.btnPrijavljivanja = new System.Windows.Forms.Button();
            this.btnZatvori = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnici)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvKorisnici
            // 
            this.dgvKorisnici.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvKorisnici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKorisnici.Location = new System.Drawing.Point(143, 30);
            this.dgvKorisnici.MultiSelect = false;
            this.dgvKorisnici.Name = "dgvKorisnici";
            this.dgvKorisnici.ReadOnly = true;
            this.dgvKorisnici.RowHeadersVisible = false;
            this.dgvKorisnici.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvKorisnici.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKorisnici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKorisnici.Size = new System.Drawing.Size(734, 417);
            this.dgvKorisnici.TabIndex = 0;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(3, 30);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(134, 33);
            this.btnDodaj.TabIndex = 1;
            this.btnDodaj.Text = "Dodaj korisnika";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(3, 69);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(134, 33);
            this.btnIzmeni.TabIndex = 2;
            this.btnIzmeni.Text = "Izmeni korisnika";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(3, 108);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(134, 33);
            this.btnObrisi.TabIndex = 3;
            this.btnObrisi.Text = "Obrisi korisnika";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnPrikaziSifre
            // 
            this.btnPrikaziSifre.Location = new System.Drawing.Point(3, 164);
            this.btnPrikaziSifre.Name = "btnPrikaziSifre";
            this.btnPrikaziSifre.Size = new System.Drawing.Size(134, 33);
            this.btnPrikaziSifre.TabIndex = 4;
            this.btnPrikaziSifre.Text = "Pregledaj sifre korisnika";
            this.btnPrikaziSifre.UseVisualStyleBackColor = true;
            this.btnPrikaziSifre.Click += new System.EventHandler(this.btnPrikaziSifre_Click);
            // 
            // btnPrikaziProfile
            // 
            this.btnPrikaziProfile.Location = new System.Drawing.Point(3, 203);
            this.btnPrikaziProfile.Name = "btnPrikaziProfile";
            this.btnPrikaziProfile.Size = new System.Drawing.Size(134, 33);
            this.btnPrikaziProfile.TabIndex = 5;
            this.btnPrikaziProfile.Text = "Korisnikovi profili";
            this.btnPrikaziProfile.UseVisualStyleBackColor = true;
            this.btnPrikaziProfile.Click += new System.EventHandler(this.btnPrikaziProfile_Click);
            // 
            // btnPrikaziPrivilegije
            // 
            this.btnPrikaziPrivilegije.Location = new System.Drawing.Point(3, 242);
            this.btnPrikaziPrivilegije.Name = "btnPrikaziPrivilegije";
            this.btnPrikaziPrivilegije.Size = new System.Drawing.Size(134, 33);
            this.btnPrikaziPrivilegije.TabIndex = 6;
            this.btnPrikaziPrivilegije.Text = "Privilegije korisnika";
            this.btnPrikaziPrivilegije.UseVisualStyleBackColor = true;
            this.btnPrikaziPrivilegije.Click += new System.EventHandler(this.btnPrikaziPrivilegije_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblPretraga);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Controls.Add(this.btnOsvezi);
            this.panel1.Controls.Add(this.btnPrijavljivanja);
            this.panel1.Controls.Add(this.btnZatvori);
            this.panel1.Controls.Add(this.btnPrikaziPrivilegije);
            this.panel1.Controls.Add(this.btnPrikaziProfile);
            this.panel1.Controls.Add(this.btnPrikaziSifre);
            this.panel1.Controls.Add(this.btnObrisi);
            this.panel1.Controls.Add(this.btnIzmeni);
            this.panel1.Controls.Add(this.btnDodaj);
            this.panel1.Controls.Add(this.dgvKorisnici);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(880, 450);
            this.panel1.TabIndex = 7;
            // 
            // lblPretraga
            // 
            this.lblPretraga.AutoSize = true;
            this.lblPretraga.Location = new System.Drawing.Point(143, 9);
            this.lblPretraga.Name = "lblPretraga";
            this.lblPretraga.Size = new System.Drawing.Size(47, 13);
            this.lblPretraga.TabIndex = 12;
            this.lblPretraga.Text = "Pretraga";
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(196, 6);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(681, 20);
            this.txtSearch.TabIndex = 11;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnOsvezi
            // 
            this.btnOsvezi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOsvezi.Location = new System.Drawing.Point(3, 375);
            this.btnOsvezi.Name = "btnOsvezi";
            this.btnOsvezi.Size = new System.Drawing.Size(134, 33);
            this.btnOsvezi.TabIndex = 10;
            this.btnOsvezi.Text = "Osvezi listu";
            this.btnOsvezi.UseVisualStyleBackColor = true;
            this.btnOsvezi.Click += new System.EventHandler(this.btnOsvezi_Click);
            // 
            // btnPrijavljivanja
            // 
            this.btnPrijavljivanja.Location = new System.Drawing.Point(3, 281);
            this.btnPrijavljivanja.Name = "btnPrijavljivanja";
            this.btnPrijavljivanja.Size = new System.Drawing.Size(134, 33);
            this.btnPrijavljivanja.TabIndex = 9;
            this.btnPrijavljivanja.Text = "Prijavljivanja korisnika";
            this.btnPrijavljivanja.UseVisualStyleBackColor = true;
            this.btnPrijavljivanja.Click += new System.EventHandler(this.btnPrijavljivanja_Click);
            // 
            // btnZatvori
            // 
            this.btnZatvori.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnZatvori.Location = new System.Drawing.Point(3, 414);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(134, 33);
            this.btnZatvori.TabIndex = 8;
            this.btnZatvori.Text = "Zatvori";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // FormUKorisnici
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 450);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(750, 450);
            this.Name = "FormUKorisnici";
            this.Text = "Uredjivanje Korisnika";
            this.Load += new System.EventHandler(this.UKorisniciForm_Load);
            this.Shown += new System.EventHandler(this.FormUKorisnici_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKorisnici)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvKorisnici;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnPrikaziSifre;
        private System.Windows.Forms.Button btnPrikaziProfile;
        private System.Windows.Forms.Button btnPrikaziPrivilegije;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnZatvori;
        private System.Windows.Forms.Button btnPrijavljivanja;
        private System.Windows.Forms.Button btnOsvezi;
        private System.Windows.Forms.Label lblPretraga;
        private System.Windows.Forms.TextBox txtSearch;
    }
}