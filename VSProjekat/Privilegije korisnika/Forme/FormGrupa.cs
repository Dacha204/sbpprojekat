﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormGrupa : Form
    {
        private RegularnaGrupaDTO grupa;
        private Utility.FormMode mode;

        public FormGrupa()
        {
            InitializeComponent();
            mode = Utility.FormMode.Create;
            grupa = new RegularnaGrupaDTO();
            PopuniPolja();
        }

        public FormGrupa(RegularnaGrupaDTO g)
        {
            InitializeComponent();
            mode = Utility.FormMode.Edit;
            grupa = g;
            PopuniPolja();
        }

        private void PopuniPolja()
        {
            tbId.Text = mode == Utility.FormMode.Create ? "<Nova Grupa>" : grupa.GrupaId.ToString();
            tbNaziv.Text = grupa.Ime;
            tbOpis.Text = grupa.Opis;
            dtpDatumKreiranja.Value = mode == Utility.FormMode.Create ? DateTime.Now : grupa.DatumKreiranja;
            if (grupa.LoginVremeOd.HasValue)
            {
                cbxLoginLimit.Checked = true;
                dtpLoginOd.Value = (DateTime) grupa.LoginVremeOd;
                dtpLoginDo.Value = (DateTime) grupa.LoginVremeDo;
            }
            else
            {
                cbxLoginLimit.Checked = false;
            }
            cbxlstIPadrese.DataSource = ProvajderPrivilegija.VratiSveIP();
            cbxlstIPadrese.DisplayMember = "IpAdresa";

            if (mode == Utility.FormMode.Create) return;

            ProvajderGrupa.UcitajIPGrupe(grupa);

            for (int i = 0; i < cbxlstIPadrese.Items.Count; i++)
            {
                var item = cbxlstIPadrese.Items[i] as IPDTO;
                if (grupa.IpAdrese.Exists(x => x.IpAdresa == item.IpAdresa))
                    cbxlstIPadrese.SetItemChecked(i, true);
            } 
        }

        private bool Validiraj()
        {
            if (this.Controls.Cast<object>().Any(c => c is TextBox && (c as TextBox).Text == String.Empty))
            {
                Utility.PraznaPoljaUpozorenje();
                return false;
            }
            return true;
        }
        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (!Validiraj()) return;

            grupa.Ime = tbNaziv.Text;
            grupa.Opis = tbOpis.Text;

            if (cbxLoginLimit.Checked)
            {
                grupa.LoginVremeOd = dtpLoginOd.Value;
                grupa.LoginVremeDo = dtpLoginDo.Value;
            }
            else
            {
                grupa.LoginVremeOd = null;
                grupa.LoginVremeDo = null;
            }

            grupa.IpAdrese = new List<IPDTO>(cbxlstIPadrese.CheckedItems.Count);
            foreach (var checkedIP in cbxlstIPadrese.CheckedItems)
            {
                grupa.IpAdrese.Add((IPDTO) checkedIP);
            }
            
            bool ops;
            if (mode == Utility.FormMode.Create)
                ops = ProvajderGrupa.NapraviGrupu(grupa);
            else
            {
                ops = ProvajderGrupa.IzmeniGrupu(grupa);
            }

            if (!ops) return;

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnOtkazi_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void tbNaziv_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }
        private void tbOpis_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void cbxLoginLimit_CheckedChanged(object sender, EventArgs e)
        {
            dtpLoginDo.Enabled = dtpLoginOd.Enabled = cbxLoginLimit.Checked;
        }
    }
}
