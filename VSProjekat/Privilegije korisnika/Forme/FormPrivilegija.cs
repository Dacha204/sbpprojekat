﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate.Util;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;
using static Privilegije_korisnika.Provajderi.Utility;

namespace Privilegije_korisnika.Forme
{
    public partial class FormPrivilegija : Form
    {
        private FormMode formMode;
        private PrivilegijaDTO privilegija;

        public FormPrivilegija()
        {
            InitializeComponent();

            formMode = FormMode.Create;
            UcitajPodatke();
        }

        public FormPrivilegija(PrivilegijaDTO p)
        {
            InitializeComponent();

            formMode = FormMode.Edit;
            privilegija = p;
            UcitajPodatke();
        }
        
        private void UcitajPodatke()
        {
            List<Type> types = new List<Type>();
            types.Add(typeof(AdminPrivilegijaDTO));
            types.Add(typeof(InterfejsPrivilegijaDTO));
            types.Add(typeof(MeniPrivilegijaDTO));
            types.Add(typeof(FunckionalnaPrivilegijaDTO));
            cmbxTip.DataSource = types;
            cmbxTip.DisplayMember = "Name";
            
            tbId.Text = formMode == FormMode.Create ? "<nova privilegija>" : privilegija.PrivilegijaId.ToString();

            gbInterfejsPrivilegija.Enabled = false;
            gbMeniPrivilegija.Enabled = false;
            if (formMode == FormMode.Create)
            {
                cmboxNadMeni.DataSource = ProvajderPrivilegija.VratiSveMoguceNadMenije();
                cmboxNadMeni.DisplayMember = "Naziv";
                return;
            }

            //editmode
            tbNaziv.Text = privilegija.Naziv;
            cmbxTip.Enabled = false;
            cmbxTip.SelectedItem = privilegija.GetType();

            if (privilegija is InterfejsPrivilegijaDTO)
            {
                //gbInterfejsPrivilegija.Enabled = true;
                cbxDozvola.Checked = ((InterfejsPrivilegijaDTO) privilegija).DozvolaKoriscenja;
            }
            if (privilegija is MeniPrivilegijaDTO)
            {
                var priv = privilegija as MeniPrivilegijaDTO;
                //gbMeniPrivilegija.Enabled = true;
                cmboxNadMeni.DataSource = ProvajderPrivilegija.VratiSveMoguceNadMenije(privilegija.PrivilegijaId);
                cmboxNadMeni.DisplayMember = "Naziv";

                if (priv.RoditeljskiMeniID != 0)
                cmboxNadMeni.SelectedItem = cmboxNadMeni.Items.Cast<PrivilegijaDTO>().ToList()
                    .Find(x => x.PrivilegijaId == priv.RoditeljskiMeniID);

            }
        }

        private bool Validiraj()
        {
            return Controls.Cast<Control>().All(c => !(c is TextBox) || c.Text != string.Empty);
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (!Validiraj())
            {
                PraznaPoljaUpozorenje();
                return;
            }

            var tip = cmbxTip.SelectedItem;
            if ((Type) tip == typeof(AdminPrivilegijaDTO))
                Sacuvaj_AdminPrivilegija();
            else if ((Type)tip == typeof(InterfejsPrivilegijaDTO))
                Sacuvaj_InterfejsPrivilegija();
            else if ((Type)tip == typeof(MeniPrivilegijaDTO))
                Sacuvaj_MeniPrivilegija();
            else
                Sacuvaj_FunkcionalnaPrivilegija();

            DialogResult = DialogResult.OK;
            Close();
            
        }

        private void Sacuvaj_AdminPrivilegija()
        {
            if (privilegija == null)
                privilegija = new AdminPrivilegijaDTO();

            privilegija.Naziv = tbNaziv.Text;
            if (formMode == FormMode.Create)
                ProvajderPrivilegija.DodajAdminPrivilegiju(privilegija as AdminPrivilegijaDTO);
            else
                ProvajderPrivilegija.AzurirajAdminPrivilegiju(privilegija as AdminPrivilegijaDTO);
        }
        private void Sacuvaj_InterfejsPrivilegija()
        {
            if (privilegija == null)
                privilegija = new InterfejsPrivilegijaDTO();
            privilegija.Naziv = tbNaziv.Text;
            (privilegija as InterfejsPrivilegijaDTO).DozvolaKoriscenja = cbxDozvola.Checked;
            if (formMode == FormMode.Create)
                ProvajderPrivilegija.DodajInterfejsPrivilegiju(privilegija as InterfejsPrivilegijaDTO);
            else 
                ProvajderPrivilegija.AzurirajInterfejsPrivilegiju(privilegija as InterfejsPrivilegijaDTO);
        }
        private void Sacuvaj_MeniPrivilegija()
        {
            if (privilegija == null)
                privilegija = new MeniPrivilegijaDTO();

            privilegija.Naziv = tbNaziv.Text;
            (privilegija as MeniPrivilegijaDTO).RoditeljskiMeniID = 
                (cmboxNadMeni.SelectedItem as MeniPrivilegijaDTO).PrivilegijaId;

            if (formMode == FormMode.Create)
                ProvajderPrivilegija.DodajMeniPrivilegiju(privilegija as MeniPrivilegijaDTO);
            else 
                ProvajderPrivilegija.AzurirajMeniPrivilegiju(privilegija as MeniPrivilegijaDTO);
        }
        private void Sacuvaj_FunkcionalnaPrivilegija()
        {
            if (privilegija == null)
                privilegija = new FunckionalnaPrivilegijaDTO();

            privilegija.Naziv = tbNaziv.Text;
            
            if (formMode == FormMode.Create)
                ProvajderPrivilegija.DodajFunkcionalnuPrivilegiju(privilegija as FunckionalnaPrivilegijaDTO);
            else
                ProvajderPrivilegija.AzurirajFunkcionalnuPrivilegiju(privilegija as FunckionalnaPrivilegijaDTO);
        }
        private void btnOtkazi_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
       
        private void FormPrivilegija_Load(object sender, EventArgs e)
        {

        }

        private void cmbxTip_SelectedIndexChanged(object sender, EventArgs e)
        {
            Type selected = cmbxTip.SelectedItem as Type;
            gbMeniPrivilegija.Enabled = false;
            gbInterfejsPrivilegija.Enabled = false;

            if (selected == typeof(InterfejsPrivilegijaDTO))
                gbInterfejsPrivilegija.Enabled = true;
            else if (selected == typeof(MeniPrivilegijaDTO))
                gbMeniPrivilegija.Enabled = true;

        }

        private void cmboxNadMeni_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
