﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormIPUpravljanje : Form
    {
        public FormIPUpravljanje()
        {
            InitializeComponent();
            OsveziListu();
        }

        private void OsveziListu()
        {
            lstbxIPadrese.DataSource = ProvajderPrivilegija.VratiSveIP();
            lstbxIPadrese.DisplayMember = "IpAdresa";
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (ProvajderPrivilegija.DodajIP(new IPDTO() {IpAdresa = txtIP.Text}))
            {
                txtIP.Text = string.Empty;
                OsveziListu();
            }

        }

        private void btnUkloni_Click(object sender, EventArgs e)
        {
            IPDTO selected = lstbxIPadrese.SelectedItem as IPDTO;

            if (selected == null)
                return;

            if (MessageBox.Show($"Obrisi {selected.IpAdresa}?",
                    "Obrisi",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No)
                return;

            ProvajderPrivilegija.IzbrisiIP(selected);
            OsveziListu();
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
