﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;
using static Privilegije_korisnika.Provajderi.Utility;

namespace Privilegije_korisnika.Forme
{
    public partial class FormProfil : Form
    {
        
        private FormMode mode;
        public ProfilDTO profil { get; protected set; }
        private List<GuiElementDTO> elementiProfila;
        public FormProfil(int kid)
        {
            InitializeComponent();
            mode = FormMode.Create;
            btnSacuvaj.Text = "Dodaj";
            profil = new ProfilDTO();
            profil.KorisnikId = kid;
            UcitajPodatke();
        }
        public FormProfil(ProfilDTO p)
        {
            InitializeComponent();
            mode = FormMode.Edit;
            profil = p;
            UcitajPodatke();
        }

        private void UcitajPodatke()
        {
            tbRedniBroj.Text = mode == FormMode.Create ? "<novi>" : profil.RedniBroj.ToString();
            tbBojaPozadine.Text = profil.BojaPozadine;
            cbxlProfilGuiEl.DataSource = ProvajderKorisnik.VratiGUIElemente();
            cbxlProfilGuiEl.DisplayMember = "Naziv";

            if (mode == FormMode.Edit)
            {
                elementiProfila = ProvajderKorisnik.VratiGUIElementeProfila(profil);
                for (int i = 0; i < cbxlProfilGuiEl.Items.Count; i++)
                {
                    var item = cbxlProfilGuiEl.Items[i] as GuiElementDTO;
                    if (elementiProfila.Exists(x => x.ElementId == item.ElementId))
                        cbxlProfilGuiEl.SetItemChecked(i, true);
                }
            }
        }

        private bool Validiraj()
        {
            if (this.Controls.Cast<object>().Any(c => c is TextBox && (c as TextBox).Text == String.Empty))
            {
                Utility.PraznaPoljaUpozorenje();
                return false;
            }
            return true;
        }
        
        #region Dogadjaji pritiska na dugme
        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            if (!Validiraj())
                return;

            profil.BojaPozadine = tbBojaPozadine.Text;
            List<GuiElementDTO> selektovani = new List<GuiElementDTO>(cbxlProfilGuiEl.CheckedItems.Count);
            selektovani.AddRange(from object checkedItem in cbxlProfilGuiEl.CheckedItems select checkedItem as GuiElementDTO);

            if (mode == FormMode.Create)
                ProvajderKorisnik.KreirajProfil(profil, selektovani);
            else
                ProvajderKorisnik.AzurirajProfil(profil, selektovani);

            DialogResult = DialogResult.OK;
            Close();
        }
        private void btnOtkazi_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        #endregion

        #region Validacije unosa

        private void tbBojaPozadine_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        #endregion

        private void btnGUIElementi_Click(object sender, EventArgs e)
        {
            FormGrafickiElementiUpravljanje fgl = new FormGrafickiElementiUpravljanje(profil);
            fgl.ShowDialog();
            UcitajPodatke();
        }
    }
}
