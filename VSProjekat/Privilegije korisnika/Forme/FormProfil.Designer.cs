﻿namespace Privilegije_korisnika.Forme
{
    partial class FormProfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbRedniBroj = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBojaPozadine = new System.Windows.Forms.TextBox();
            this.btnSacuvaj = new System.Windows.Forms.Button();
            this.btnOtkazi = new System.Windows.Forms.Button();
            this.btnGUIElementiUpravljac = new System.Windows.Forms.Button();
            this.cbxlProfilGuiEl = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbRedniBroj
            // 
            this.tbRedniBroj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRedniBroj.Location = new System.Drawing.Point(88, 12);
            this.tbRedniBroj.Name = "tbRedniBroj";
            this.tbRedniBroj.ReadOnly = true;
            this.tbRedniBroj.Size = new System.Drawing.Size(305, 20);
            this.tbRedniBroj.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Redni broj";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Boja pozadine";
            // 
            // tbBojaPozadine
            // 
            this.tbBojaPozadine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBojaPozadine.Location = new System.Drawing.Point(88, 38);
            this.tbBojaPozadine.Name = "tbBojaPozadine";
            this.tbBojaPozadine.Size = new System.Drawing.Size(305, 20);
            this.tbBojaPozadine.TabIndex = 3;
            this.tbBojaPozadine.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBojaPozadine_KeyPress);
            // 
            // btnSacuvaj
            // 
            this.btnSacuvaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSacuvaj.Location = new System.Drawing.Point(197, 324);
            this.btnSacuvaj.Name = "btnSacuvaj";
            this.btnSacuvaj.Size = new System.Drawing.Size(95, 33);
            this.btnSacuvaj.TabIndex = 4;
            this.btnSacuvaj.Text = "Sačuvaj";
            this.btnSacuvaj.UseVisualStyleBackColor = true;
            this.btnSacuvaj.Click += new System.EventHandler(this.btnSacuvaj_Click);
            // 
            // btnOtkazi
            // 
            this.btnOtkazi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOtkazi.Location = new System.Drawing.Point(298, 324);
            this.btnOtkazi.Name = "btnOtkazi";
            this.btnOtkazi.Size = new System.Drawing.Size(95, 33);
            this.btnOtkazi.TabIndex = 5;
            this.btnOtkazi.Text = "Otkaži";
            this.btnOtkazi.UseVisualStyleBackColor = true;
            this.btnOtkazi.Click += new System.EventHandler(this.btnOtkazi_Click);
            // 
            // btnGUIElementiUpravljac
            // 
            this.btnGUIElementiUpravljac.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGUIElementiUpravljac.Location = new System.Drawing.Point(15, 324);
            this.btnGUIElementiUpravljac.Name = "btnGUIElementiUpravljac";
            this.btnGUIElementiUpravljac.Size = new System.Drawing.Size(176, 33);
            this.btnGUIElementiUpravljac.TabIndex = 6;
            this.btnGUIElementiUpravljac.Text = "Upravljanje GUI Elementima...";
            this.btnGUIElementiUpravljac.UseVisualStyleBackColor = true;
            this.btnGUIElementiUpravljac.Click += new System.EventHandler(this.btnGUIElementi_Click);
            // 
            // cbxlProfilGuiEl
            // 
            this.cbxlProfilGuiEl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxlProfilGuiEl.CheckOnClick = true;
            this.cbxlProfilGuiEl.FormattingEnabled = true;
            this.cbxlProfilGuiEl.Location = new System.Drawing.Point(15, 86);
            this.cbxlProfilGuiEl.Name = "cbxlProfilGuiEl";
            this.cbxlProfilGuiEl.Size = new System.Drawing.Size(378, 229);
            this.cbxlProfilGuiEl.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "GUI Elementi dodeljeni profilu";
            // 
            // FormProfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 369);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbxlProfilGuiEl);
            this.Controls.Add(this.btnGUIElementiUpravljac);
            this.Controls.Add(this.btnOtkazi);
            this.Controls.Add(this.btnSacuvaj);
            this.Controls.Add(this.tbBojaPozadine);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbRedniBroj);
            this.MinimumSize = new System.Drawing.Size(421, 408);
            this.Name = "FormProfil";
            this.Text = "Profil";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbRedniBroj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBojaPozadine;
        private System.Windows.Forms.Button btnSacuvaj;
        private System.Windows.Forms.Button btnOtkazi;
        private System.Windows.Forms.Button btnGUIElementiUpravljac;
        private System.Windows.Forms.CheckedListBox cbxlProfilGuiEl;
        private System.Windows.Forms.Label label3;
    }
}