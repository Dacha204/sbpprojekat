﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Forme
{
    public partial class FormPrijave : Form
    {
        public KorisnikDTO korisnik;
        public FormPrijave(KorisnikDTO korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            UcitajPodaci();
        }

        private void UcitajPodaci()
        {
            dgvPrijave.DataSource = ProvajderKorisnik.VratiPrijaveKorisnika(korisnik);
        }

        private void btnZatvori_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
