﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Provajderi.DTO;
using Privilegije_korisnika.Provajderi;

namespace Privilegije_korisnika.Forme
{
    public partial class FormDodavanjeUGrupu : Form
    {
        public enum DodajMod
        {
            Grupu,
            Korisnika
        }

        private DodajMod mod;
        private RegularnaGrupaDTO  grupa;

        public FormDodavanjeUGrupu(RegularnaGrupaDTO g, DodajMod m)
        {
            InitializeComponent();
            grupa = g;
            mod = m;
            OsveziListu();
        }

        private void FormDodavanjeUGrupu_Load(object sender, EventArgs e)
        {
           
        }

        private void OsveziListu()
        {
            
            if (mod == DodajMod.Grupu)
            {
                List<RegularnaGrupaDTO> list = ProvajderGrupa.SveRegularneGrupe();
                List<RegularnaGrupaDTO> sadrzivec = ProvajderGrupa.RGrupeUGrupi(grupa);
                list.RemoveAll(x => sadrzivec.Exists(y => y.GrupaId == x.GrupaId));
                list.Remove(list.Find(x=>x.GrupaId == grupa.GrupaId));
                dgvDodavanje.DataSource = list;
            }
            else
            {
                List<KorisnickaGrupaDTO> list = ProvajderGrupa.SveKorisnickeGrupe();
                List<KorisnickaGrupaDTO> sadrzivec = ProvajderGrupa.KGrupeUGrupi(grupa);
                list.RemoveAll(x => sadrzivec.Exists(y => y.GrupaId == x.GrupaId));
                dgvDodavanje.DataSource = list;
            }
        }

        private void btnSacuvaj_Click(object sender, EventArgs e)
        {
            bool opstat = false;
            if (mod == DodajMod.Grupu)
            {
                RegularnaGrupaDTO grp = dgvDodavanje.SelectedRows[0].DataBoundItem as RegularnaGrupaDTO;
                opstat = ProvajderGrupa.DodajGrupuUGrupu(grupa, grp);
            }
            else
            {
                KorisnickaGrupaDTO krp = dgvDodavanje.SelectedRows[0].DataBoundItem as KorisnickaGrupaDTO;
                opstat = ProvajderGrupa.DodajKorisnikaUGrupu(grupa, krp);
            }

            if (opstat)
            {
                Close();
                return;
            }

            Utility.PrikaziGresku("Neuspelo dodavanje u grupi!");
        }

        private void btnOtkazi_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
