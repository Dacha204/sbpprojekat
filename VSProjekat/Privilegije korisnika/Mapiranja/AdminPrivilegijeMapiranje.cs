﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class AdminPrivilegijeMapiranje : SubclassMap<AdminPrivilegija>
    {
        public AdminPrivilegijeMapiranje()
        {
            Table("PRJ_ADMIN_PRIVILEGIJA");
            KeyColumn("PrivilegijaID");
        }

    }
}
