﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class RegularnaGrupaMapiranje : SubclassMap<RegularnaGrupa>
    {
        public RegularnaGrupaMapiranje()
        {
            DiscriminatorValue(0);

            HasManyToMany<Grupa>(Reveal.Member<RegularnaGrupa>("Sadrzi"))
                .Table("PRJ_PRIPADA_GRUPI")
                .ParentKeyColumn("GRoditeljID")
                .ChildKeyColumn("GDeteID");
        }
    }
}
