﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class GrupaMapiranje : ClassMap<Grupa>
    {
        public GrupaMapiranje()
        {
            Table("PRJ_GRUPA");

            Id(x => x.GrupaId).Column("GrupaID").GeneratedBy.TriggerIdentity();

            Map(x => x.Ime).Column("Ime").Not.Nullable().Unique();
            Map(x => x.Opis).Column("Opis").Not.Nullable();
            Map(x => x.DatumKreiranja).Column("DatumKreiranja").Not.Nullable();
            Map(x => x.LoginVremeOd).Column("LoginVremeOd");
            Map(x => x.LoginVremeDo).Column("LoginVremeDo");
            DiscriminateSubClassesOnColumn("KorisnickaFlag").Not.Nullable();

            HasMany(x => x.ImaDodelePrivilegija)
                .Table("PRJ_DODELA_PRIVILEGIJA")
                .KeyColumn("GrupaID")
                .Cascade.SaveUpdate()
                .Inverse();

            HasManyToMany(x => x.IpAdrese)
                .Table("PRJ_DOZVOLJENE_IP")
                .ParentKeyColumn("GrupaID")
                .ChildKeyColumn("IpAdresa");

        }
    }
}
