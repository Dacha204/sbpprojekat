﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class KorisnickaGrupaMapiranje : SubclassMap<KorisnickaGrupa>
    {
        public KorisnickaGrupaMapiranje()
        {
            DiscriminatorValue(1);

            HasOne(x => x.Korisnik).PropertyRef("KorisnickaGrupa");

            HasManyToMany(x => x.PripadaGrupama)
                .Table("PRJ_PRIPADA_GRUPI")
                .ParentKeyColumn("GDeteID")
                .ChildKeyColumn("GRoditeljID")
                .Cascade.SaveUpdate()
                .Inverse();
        }
    }
}
