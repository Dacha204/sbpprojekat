﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using FluentNHibernate.MappingModel;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class PrijavljivanjeMapiranje : ClassMap<Prijavljivanje>
    {
        public PrijavljivanjeMapiranje()
        {
            Table("PRJ_PRIJAVLJIVANJE");

            Id(x => x.PrijavaId, "PrijavaID").GeneratedBy.TriggerIdentity();

            Map(x => x.Uspesna, "UspesnoFlag");
            Map(x => x.IpAdresa, "IPAddresa");
            Map(x => x.Vreme, "Vreme");
            Map(x => x.Sifra, "Sifra");

            References(x => x.Korisnik, "KorisnikID").Not.Nullable();
        }
    }

}
