﻿namespace Privilegije_korisnika
{
    partial class FormGlavna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUredjivanjeKorisnika = new System.Windows.Forms.Button();
            this.btnUredjivanjeGrupe = new System.Windows.Forms.Button();
            this.btnPrijava = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrivilegije = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnUredjivanjeKorisnika
            // 
            this.btnUredjivanjeKorisnika.Location = new System.Drawing.Point(10, 81);
            this.btnUredjivanjeKorisnika.Name = "btnUredjivanjeKorisnika";
            this.btnUredjivanjeKorisnika.Size = new System.Drawing.Size(153, 55);
            this.btnUredjivanjeKorisnika.TabIndex = 12;
            this.btnUredjivanjeKorisnika.Text = "Uredjivanje korisnika";
            this.btnUredjivanjeKorisnika.UseVisualStyleBackColor = true;
            this.btnUredjivanjeKorisnika.Click += new System.EventHandler(this.btnUredjivanjeKorisnika_Click);
            // 
            // btnUredjivanjeGrupe
            // 
            this.btnUredjivanjeGrupe.Location = new System.Drawing.Point(328, 81);
            this.btnUredjivanjeGrupe.Name = "btnUredjivanjeGrupe";
            this.btnUredjivanjeGrupe.Size = new System.Drawing.Size(153, 55);
            this.btnUredjivanjeGrupe.TabIndex = 13;
            this.btnUredjivanjeGrupe.Text = "Uredjivanje grupa";
            this.btnUredjivanjeGrupe.UseVisualStyleBackColor = true;
            this.btnUredjivanjeGrupe.Click += new System.EventHandler(this.btnUredjivanjeGrupe_Click);
            // 
            // btnPrijava
            // 
            this.btnPrijava.Location = new System.Drawing.Point(169, 81);
            this.btnPrijava.Name = "btnPrijava";
            this.btnPrijava.Size = new System.Drawing.Size(153, 55);
            this.btnPrijava.TabIndex = 14;
            this.btnPrijava.Text = "Prijavi se";
            this.btnPrijava.UseVisualStyleBackColor = true;
            this.btnPrijava.Click += new System.EventHandler(this.btnPrijava_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(294, 31);
            this.label1.TabIndex = 15;
            this.label1.Text = "Sistemi Baza Podataka";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(245, 31);
            this.label2.TabIndex = 16;
            this.label2.Text = "Privilegija korisnika";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnPrivilegije
            // 
            this.btnPrivilegije.Location = new System.Drawing.Point(487, 81);
            this.btnPrivilegije.Name = "btnPrivilegije";
            this.btnPrivilegije.Size = new System.Drawing.Size(153, 55);
            this.btnPrivilegije.TabIndex = 17;
            this.btnPrivilegije.Text = "Privilegije";
            this.btnPrivilegije.UseVisualStyleBackColor = true;
            this.btnPrivilegije.Click += new System.EventHandler(this.btnPrivilegije_Click);
            // 
            // FormGlavna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 151);
            this.Controls.Add(this.btnPrivilegije);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPrijava);
            this.Controls.Add(this.btnUredjivanjeGrupe);
            this.Controls.Add(this.btnUredjivanjeKorisnika);
            this.MaximumSize = new System.Drawing.Size(670, 190);
            this.MinimumSize = new System.Drawing.Size(509, 190);
            this.Name = "FormGlavna";
            this.Text = "SBP: Privilegije Korisnika";
            this.Load += new System.EventHandler(this.FormGlavna_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnUredjivanjeKorisnika;
        private System.Windows.Forms.Button btnUredjivanjeGrupe;
        private System.Windows.Forms.Button btnPrijava;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPrivilegije;
    }
}

