﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Linq;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Forme;
using Privilegije_korisnika.Provajderi;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika
{
    public partial class FormGlavna : Form
    {
        private string prikaz;
        public FormGlavna()
        {
            InitializeComponent();
            prikaz = string.Empty;
        }

        public void IzvuciExceptionPorukeIPrikazi(Exception ex)
        {
            string poruke = ex.Message;
            Exception exx = ex;
            while (exx.InnerException != null)
            {
                poruke += "\n\n" + exx.InnerException.Message;
                exx = exx.InnerException;
            }
            MessageBox.Show(poruke, "Izuzetak");
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Korisnik k = s.Load<Korisnik>(7);
        //        prikaz = string.Format("ID: {0}\n" +
        //                               "Ime: {1}\n" +
        //                               "Prezime: {2}\n" +
        //                               "Ime roditelja: {3}\n" +
        //                               "Korisnicko ime: {4}\n" +
        //                               "JMBG: {5}\n" +
        //                               "Datum rodjenja: {6}\n" +
        //                               "Radno mesto: {7}\n" +
        //                               "Funkcija: {8}\n" +
        //                               "Broj kancelarije: {9}\n" +
        //                               "Email: {10}\n" +
        //                               "Telefon: {11}",
        //            k.KorisnikId,
        //            k.Ime,
        //            k.Prezime,
        //            k.ImeRoditelja,
        //            k.KorisnickoIme,
        //            k.Jmbg,
        //            k.DatumRodjenja,
        //            k.RadnoMesto,
        //            k.Funkcija,
        //            k.BrojKancelarije,
        //            k.Email,
        //            k.Telefon);

        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }
        //}

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        KorisnickaGrupa kg = s.Load<KorisnickaGrupa>(15);
        //        RegularnaGrupa rg = s.Load<RegularnaGrupa>(5);

        //        prikaz = string.Format("Naziv grupe: {0}\n" +
        //                               "ID: {1}\n" +
        //                               "Opis: {2}\n" +
        //                               "Datum kreiranja : {3}\n" +
        //                               "Login vreme od: {4}\n" +
        //                               "Do: {5}\n" +
        //                               "Korisnicka: {6}",
        //            kg.Ime,
        //            kg.GrupaId,
        //            kg.Opis,
        //            kg.DatumKreiranja,
        //            kg.LoginVremeOd,
        //            kg.LoginVremeDo,
        //            "da");
        //        MessageBox.Show(prikaz);

        //        prikaz = string.Format("Naziv grupe: {0}\n" +
        //                               "ID: {1}\n" +
        //                               "Opis: {2}\n" +
        //                               "Datum kreiranja : {3}\n" +
        //                               "Login vreme od: {4}\n" +
        //                               "Do: {5}\n" +
        //                               "Korisnicka: {6}",
        //            rg.Ime,
        //            rg.GrupaId,
        //            rg.Opis,
        //            rg.DatumKreiranja,
        //            rg.LoginVremeOd,
        //            rg.LoginVremeDo,
        //            "ne");

        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }
        //}

        //private void button3_Click(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Ip ip = s.Load<Ip>("10.10.0.1");

        //        prikaz = ip.IpAdresa;
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }
        //}

        //private void button4_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Sifre sf = s.Load<Sifre>(29);

        //        string trenutna = sf.Trenutna ? "Da" : "Ne";
        //        prikaz = string.Format("ID :{0}\n" +
        //                               "Sifra: {1}\n" +
        //                               "Datum postavljanja: {2}\n" +
        //                               "Trenutna: {3}",
        //            sf.SifraId,
        //            sf.Sifra, 
        //            sf.DatumPostavljanja, 
        //            trenutna);
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }

        //}

        //private void button5_Click(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Prijavljivanje p = s.Load<Prijavljivanje>(1);

        //        string uspesno = p.Uspesna ? "Da" : "Ne";
        //        prikaz = string.Format("ID prijave: {0}\n" +
        //                               "Uspesno: {1}\n" +
        //                               "Vreme: {2}\n" +
        //                               "Sifra: {3}\n" +
        //                               "IPAdresa: {4}",
        //            p.PrijavaId,
        //            uspesno, 
        //            p.Vreme, 
        //            p.Sifra,
        //            p.IpAdresa);
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }

        //}

        //private void button9_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        MeniPrivilegija mp = s.Load<MeniPrivilegija>(15);
        //        AdminPrivilegija ap = s.Load<AdminPrivilegija>(19);
        //        FunkcionalnaPrivilegija fp = s.Load<FunkcionalnaPrivilegija>(10);
        //        InterfejsPrivilegija ip = s.Load<InterfejsPrivilegija>(2);

        //        prikaz = string.Format("ID meni privilegije: {0}\n" +
        //                               "ID interfejs privilegije: {1}\n" +
        //                               "ID funkcionalne privilegije: {2}\n" +
        //                               "ID admin privilegije: {3}",
        //            mp.PrivilegijaId,
        //            ip.PrivilegijaId,
        //            fp.PrivilegijaId,
        //            ap.PrivilegijaId );
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
               
        //    }
        //}

        //private void button6_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        GuiElement gui = s.Load<GuiElement>(1);

        //        prikaz = string.Format("ID: {0}\n" +
        //                               "Opis: {1}",
        //            gui.ElementId,
        //            gui.Naziv);
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }
        //}

        //private void button7_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Profil p = s.Load<Profil>(3);

        //        prikaz = string.Format("Redni broj: {0}\n" +
        //                               "Boja pozadine: {1}", 
        //            p.RedniBroj,
        //            p.BojaPozadine);
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }
        //}

        //private void button8_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ISession s = DataLayer.GetSession();

        //        Privilegija p = s.Load<Privilegija>(1);

        //        prikaz = string.Format("ID: {0}\n" +
        //                               "Naziv: {1}", 
        //            p.PrivilegijaId,
        //            p.Naziv);
        //        MessageBox.Show(prikaz);

        //        s.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //    }
        //}

        //private void button10_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string kIme = "Cecko " + DateTime.Now.Millisecond;
        //        ISession s = DataLayer.GetSession();

        //        RegularnaGrupa g = s.Load<RegularnaGrupa>(1);

        //        //kreiranje korisnika
        //        Korisnik k = new Korisnik()
        //        {
        //            Ime = "Stefan",
        //            Prezime = "Djordjevic",
        //            KorisnickoIme = kIme,
        //            Email = "mail@mail.com",
        //            ImeRoditelja = "Lazar",
        //            Jmbg = "123456",
        //            DatumRodjenja = DateTime.Now,
                
        //        };
        //        //kreiranje enkapsulirajuce grupe za korisnika
        //        KorisnickaGrupa kg = new KorisnickaGrupa()
        //        {
        //            DatumKreiranja = DateTime.Now,
        //            Ime = kIme,
        //            Opis = "Korisnik:"+kIme
        //        };
        //        s.Save(kg);
        //        k.KorisnickaGrupa = kg;
        //        s.Save(k);

        //        Sifre sf = new Sifre()
        //        {
        //            Sifra = "celticsNo1",
        //            DatumPostavljanja = DateTime.Now,
        //            Korisnik = k
        //        };

        //        s.Save(sf);

        //        s.Flush();

        //        IList<Korisnik> rezultat = s.Query<Korisnik>()
        //            .Where(p => p.KorisnickoIme == kIme )
        //            .Select(p => p).ToList();

        //        Korisnik korisnik = rezultat[0];
           
        //        prikaz = string.Format("ID: {0}\n" +
        //                               "Ime: {1}\n" +
        //                               "Prezime: {2}\n" +
        //                               "Korisnicko ime: {3}",
        //            korisnik.KorisnikId,
        //            korisnik.Ime, 
        //            korisnik.Prezime, 
        //            korisnik.KorisnickoIme);
        //        MessageBox.Show(prikaz);

        //        IList<Sifre> sifreKorisnika = s.Query<Sifre>()
        //            .Where(p => p.Korisnik == korisnik)
        //            .Select(p => p).ToList();
        //        foreach (Sifre sifra in sifreKorisnika)
        //        {
        //            s.Delete(sifra);
        //        }
        //        s.Delete(korisnik);
        //        s.Flush();
        //        s.Close();
        //        MessageBox.Show("Novokreirani korisnik uspesno obrisan");
        //    }
        //    catch (Exception ex)
        //    {
        //        IzvuciExceptionPorukeIPrikazi(ex);
        //        throw;
        //    }

        //}

        //private void button11_Click(object sender, EventArgs e)
        //{
        //    ISession s = DataLayer.GetSession();
        //    Korisnik k = s.Load<Korisnik>(6);

        //    foreach (var sifra in k.Sifre)
        //    {
        //        prikaz = string.Format("Sifra: {0}\n" +
        //                               "ID sifre: {1}",
        //                               sifra.Sifra, 
        //                               sifra.SifraId);
        //        MessageBox.Show(prikaz);
        //    }


        //    s.Close();

        //}

        //private void button12_Click(object sender, EventArgs e)
        //{
        //    ISession s = DataLayer.GetSession();

        //    Korisnik k1 = s.Load<Korisnik>(8);
        //    Prijavljivanje p = new Prijavljivanje()
        //    {
        //        IpAdresa = "10.4.0.1",
        //        Uspesna = true,
        //        Vreme = DateTime.Now,
        //        Sifra = "proba",
        //        Korisnik = k1
        //    };

        //    k1.Prijave.Add(p);

        //    s.Save(p);
        //    s.Save(k1);
        //    s.Flush();

        //    Korisnik k = s.Load<Korisnik>(8);

        //    foreach (var pr in k.Prijave)
        //    {
        //        MessageBox.Show("IP sa kojih se korisnik prijavljivao:" + pr.IpAdresa);
        //    }

        //    s.Close();

        //}

        private void FormGlavna_Load(object sender, EventArgs e)
        {

        }

        private void btnUredjivanjeKorisnika_Click(object sender, EventArgs e)
        {
            FormUKorisnici uf = new FormUKorisnici();
            uf.ShowDialog();

        }

        private void btnUredjivanjeGrupe_Click(object sender, EventArgs e)
        {
            FormGrupe fg = new FormGrupe();
            fg.ShowDialog();
        }

        private void btnPrijava_Click(object sender, EventArgs e)
        {
            FormPrijava fp = new FormPrijava();
            fp.ShowDialog();
        }

        private void btnPrivilegije_Click(object sender, EventArgs e)
        {
            FormPrivilegije fp = new FormPrivilegije();
            fp.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
