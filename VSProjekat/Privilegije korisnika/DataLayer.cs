﻿using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;


namespace Privilegije_korisnika
{
    public class DataLayer
    {
        private static ISessionFactory _factory = null;
        private static object objLock = new object();
        public static ISession Sesija { get; protected set; }

        public static ISession OtvoriSesiju()
        {
            if (Sesija != null)
                Sesija.Close();

            Sesija = GetSession();
            return Sesija;
        }

        public static void ZatvoriSesiju()
        {
            if (Sesija != null)
                Sesija.Close();

            Sesija = null;
        }

        //funkcija na zahtev otvara sesiju
        protected static ISession GetSession()
        {
            //ukoliko session factory nije kreiran
            if (_factory == null)
            {
                lock (objLock)
                {
                    if (_factory == null)
                        _factory = CreateSessionFactory();
                }
            }

            return _factory.OpenSession();
        }

        //konfiguracija i kreiranje session factory
        private static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var cfg = OracleManagedDataClientConfiguration.Oracle10
                    .ShowSql()
                    .ConnectionString(c =>
                        c.Is("Data Source=gislab-oracle.elfak.ni.ac.rs:1521/SBP_PDB;User Id=S15024;Password=palatalizacija"));

                return Fluently.Configure()
                    .Database(cfg)
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                    .BuildSessionFactory();
            }
            catch (Exception ec)
            {
                System.Windows.Forms.MessageBox.Show(ec.Message);
                return null;
            }

        }
    }
}
