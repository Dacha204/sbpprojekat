﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Provajderi
{
    public static class Utility
    {
        public enum FormMode
        {
            Create,
            Edit
        }

        public static void PrikaziIzuetak(Exception e)
        {
            MessageBox.Show(e.Message, "Izuzetak", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static void PraznaPoljaUpozorenje()
        {
            MessageBox.Show("Morate popuniti sva polja", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static void NevalidanMail()
        {
            MessageBox.Show("Niste uneli email u pravilnom formatu", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static void PrikaziGresku(string poruka)
        {
            MessageBox.Show(poruka, "Greska", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        
    }
}
