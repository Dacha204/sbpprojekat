﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Provajderi
{
    public class ProvajderPrivilegija
    {
        public static List<IPDTO> VratiSveIP()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                return DTOGenerator.IpDtoListaGenerator(ses.Query<Ip>().Select(x => x).ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return new List<IPDTO>(1);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static IPDTO VratiIP(string ipkey)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Ip ip = ses.Load<Ip>(ipkey);
                return DTOGenerator.IpDtoGenerator(ip);
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool DodajIP(IPDTO ipd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Ip ip = new Ip()
                {
                    IpAdresa = ipd.IpAdresa
                };


                ses.SaveOrUpdate(ip);
                ses.Flush();
                return true;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool IzbrisiIP(IPDTO ipd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Ip ip = ses.Load<Ip>(ipd.IpAdresa);

                ses.Delete(ip);
                ses.Flush();
                return true;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<IPDTO> VratiIPKorisnika(int kid)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kid);
                return DTOGenerator.IpDtoListaGenerator(k.KorisnickaGrupa.IpAdrese.ToList());
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool AzurirajIPKorisnika(int kid, List<IPDTO> ipl)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kid);
                
                k.KorisnickaGrupa.IpAdrese = new List<Ip>(ipl.Count);
                
                foreach (IPDTO ipdto in ipl)
                {
                    k.KorisnickaGrupa.IpAdrese.Add(ses.Load<Ip>(ipdto.IpAdresa));
                }
                
                ses.SaveOrUpdate(k);
                ses.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool AzurirajIPGrupe(RegularnaGrupa rd, List<IPDTO> ipd)
        {
            try
            {
                ISession ses = DataLayer.Sesija;
                IList<Ip> noveIP = new List<Ip>(ipd.Count);
                foreach (IPDTO ipdto in ipd)
                {
                    noveIP.Add(ses.Load<Ip>(ipdto.IpAdresa));
                }

                rd.IpAdrese = noveIP;
                ses.SaveOrUpdate(rd);
                ses.Flush();
                return true;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return false;
            }

        }

        public static List<PrivilegijaDTO> SvePrivilegije()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<Privilegija> privilegije = ses.Query<Privilegija>().Select(x => x).ToList();
                //NHibernateUtil.Initialize(privilegije);
                return DTOGenerator.PrivilegijeListDTOGenerator(privilegije);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajInterfejsPrivilegiju(InterfejsPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                InterfejsPrivilegija pr = new InterfejsPrivilegija()
                {
                    Naziv = p.Naziv,
                    DozvolaKoriscenja = p.DozvolaKoriscenja
                };
                ses.Save(pr);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajAdminPrivilegiju(AdminPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                AdminPrivilegija pr = new AdminPrivilegija()
                {
                    Naziv = p.Naziv
                };
                ses.Save(pr);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajFunkcionalnuPrivilegiju(FunckionalnaPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                FunkcionalnaPrivilegija pr = new FunkcionalnaPrivilegija()
                {
                    Naziv = p.Naziv
                };
                
                ses.Save(pr);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);

            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<MeniPrivilegijaDTO> VratiSveMoguceNadMenije(int meniID = 0)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<MeniPrivilegija> mpriv = ses.Query<MeniPrivilegija>()
                    .Select(x => x)
                    .Where(x => x.PrivilegijaId != meniID)
                    .ToList();

                List<MeniPrivilegijaDTO> rez = DTOGenerator.PrivilegijeListDTOGenerator(
                    mpriv.Cast<Privilegija>().ToList())
                    .Cast<MeniPrivilegijaDTO>().ToList();
                rez.Insert(0,new MeniPrivilegijaDTO(){Naziv = "Nema", PrivilegijaId = 0});
                return rez;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajMeniPrivilegiju(MeniPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                MeniPrivilegija pr = new MeniPrivilegija()
                {
                    Naziv = p.Naziv
                };

                if (p.RoditeljskiMeniID == 0)
                    pr.RoditeljskiMeni = null;
                else
                {
                    pr.RoditeljskiMeni = ses.Load<MeniPrivilegija>(p.RoditeljskiMeniID);
                }
                      
                ses.Save(pr);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);

            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static PrivilegijaDTO VratiPrivilegiju(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Privilegija p = ses.Load<Privilegija>(id);

                return DTOGenerator.PrivilegijaDTOGenerator(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static void AzurirajMeniPrivilegiju(MeniPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                MeniPrivilegija pe = ses.Load<MeniPrivilegija>(p.PrivilegijaId);
                pe.Naziv = p.Naziv;
                if (p.RoditeljskiMeniID == 0)
                    pe.RoditeljskiMeni = null;
                else
                {
                    pe.RoditeljskiMeni = ses.Load<MeniPrivilegija>(p.RoditeljskiMeniID);
                }
                ses.Update(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajAdminPrivilegiju(AdminPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                AdminPrivilegija pe = ses.Load<AdminPrivilegija>(p.PrivilegijaId);
                pe.Naziv = p.Naziv;
                ses.Update(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajFunkcionalnuPrivilegiju(FunckionalnaPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                FunkcionalnaPrivilegija pe = ses.Load<FunkcionalnaPrivilegija>(p.PrivilegijaId);
                pe.Naziv = p.Naziv;
                ses.Update(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajInterfejsPrivilegiju(InterfejsPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                InterfejsPrivilegija pe = ses.Load<InterfejsPrivilegija>(p.PrivilegijaId);
                pe.Naziv = p.Naziv;
                pe.DozvolaKoriscenja = p.DozvolaKoriscenja;
                ses.Update(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static void ObrisiInterfejsPrivilegiju(InterfejsPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                InterfejsPrivilegija pe = ses.Load<InterfejsPrivilegija>(p.PrivilegijaId);
                ses.Delete(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiMeniPrivilegiju(MeniPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                MeniPrivilegija pe = ses.Load<MeniPrivilegija>(p.PrivilegijaId);
                ses.Delete(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiAdminPrivilegiju(AdminPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                AdminPrivilegija pe = ses.Load<AdminPrivilegija>(p.PrivilegijaId);
                ses.Delete(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiFunckionalnuPrivilegiju(FunckionalnaPrivilegijaDTO p)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                FunkcionalnaPrivilegija pe = ses.Load<FunkcionalnaPrivilegija>(p.PrivilegijaId);
                ses.Delete(pe);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static AdminPrivilegijaDTO VratiAdminPrivilegiju(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                AdminPrivilegija p = ses.Load<AdminPrivilegija>(id);

                return DTOGenerator.DtoAdmin(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static MeniPrivilegijaDTO VratiMeniPrivilegiju(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                MeniPrivilegija p = ses.Load<MeniPrivilegija>(id);

                return DTOGenerator.DtoMeni(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static InterfejsPrivilegijaDTO VratiInterfejsPrivilegiju(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                InterfejsPrivilegija p = ses.Load<InterfejsPrivilegija>(id);

                return DTOGenerator.DtoInterfejs(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static FunckionalnaPrivilegijaDTO VratiFunckionalnuPrivilegiju(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                FunkcionalnaPrivilegija p = ses.Load<FunkcionalnaPrivilegija>(id);

                return DTOGenerator.DtoFunckionalna(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        private static List<PrivilegijaDTO> SvePrivilegijeG(Grupa g)
        {
            try
            {
                ISession ses =  DataLayer.Sesija;
                List<PrivilegijaDTO> privilegije =
                    new List<PrivilegijaDTO>(g.ImaDodelePrivilegija.Count);

                foreach (var dodela in g.ImaDodelePrivilegija)
                {
                    Privilegija p = ses.Get<Privilegija>(dodela.DodelaID.PrivilegijaID.PrivilegijaId);
                    //NHibernateUtil.Initialize(p);
                    privilegije.Add(DTOGenerator.PrivilegijaDTOGenerator(p));
                }
                return privilegije;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
        }
        public static List<PrivilegijaDTO> SveKorisnikPrivilegije(KorisnikDTO kd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kd.KorisnikId);
                return SvePrivilegijeG(k.KorisnickaGrupa);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<PrivilegijaDTO> SveGrupaPrivilegije(GrupaDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa k = ses.Load<RegularnaGrupa>(gd.GrupaId);
                return SvePrivilegijeG(k);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static void AzurirajPrivilegijeKorisnika(KorisnikDTO kd, List<PrivilegijaDTO> pl, KorisnikDTO dodelio)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kd.KorisnikId);
                Korisnik kdodelio = ses.Load<Korisnik>(dodelio.KorisnikId);
                DodeliPrivilegije(k.KorisnickaGrupa, pl, kdodelio);
                ses.SaveOrUpdate(k);
                ses.Flush();
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajPrivilegijeGrupa(RegularnaGrupaDTO g, List<PrivilegijaDTO> pl, KorisnikDTO dodelio)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa r = ses.Load<RegularnaGrupa>(g.GrupaId);
                Korisnik kdodelio = ses.Load<Korisnik>(dodelio.KorisnikId);
                DodeliPrivilegije(r, pl, kdodelio);
                ses.SaveOrUpdate(r.ImaDodelePrivilegija);
                ses.Flush();
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        private static void DodeliPrivilegije(Grupa g, List<PrivilegijaDTO> pl, Korisnik dodelio)
        {
            try
            {
                
                List<DodelaPrivilegijeVeza> zaUklanjanje = new List<DodelaPrivilegijeVeza>();
                foreach (DodelaPrivilegijeVeza dodela in g.ImaDodelePrivilegija)
                {
                    if (!pl.Exists(x=>x.PrivilegijaId == dodela.DodelaID.PrivilegijaID.PrivilegijaId))
                        zaUklanjanje.Add(dodela);
                }
                foreach (DodelaPrivilegijeVeza rm in zaUklanjanje)
                {
                    g.ImaDodelePrivilegija.Remove(rm);
                    DataLayer.Sesija.Delete(rm);
                }
                

                foreach (PrivilegijaDTO pdto in pl)
                {
                    bool nema = g.ImaDodelePrivilegija.All(x => x.DodelaID.PrivilegijaID.PrivilegijaId != pdto.PrivilegijaId);
                    if (nema)
                        DodajPrivilegiju(g, pdto, dodelio);
                }
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
           
        }
        private static void DodajPrivilegiju(Grupa g, PrivilegijaDTO pd, Korisnik dodelio)
        {
            try
            {
                ISession ses = DataLayer.Sesija;
                Privilegija p = ses.Load<Privilegija>(pd.PrivilegijaId);

                DodelaPrivilegijeVeza d = new DodelaPrivilegijeVeza();
                d.DodelaID.GrupaID = g;
                d.DodelaID.PrivilegijaID = p;
                d.DodelioKorisnik = dodelio;
                d.DatumDodele = DateTime.Now;

                ses.SaveOrUpdate(d);
                ses.SaveOrUpdate(g);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }

        }
    }
}
