﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Provajderi
{
    public static class DTOGenerator
    {
        public static KorisnikDTO KorisnikDTOGenerator(Korisnik k, bool NeUcitavajSifru = false)
        {
            KorisnikDTO kr = new KorisnikDTO()
            {
                KorisnikId = k.KorisnikId,
                KorisnickoIme = k.KorisnickoIme,
                Ime = k.Ime,
                ImeRoditelja = k.ImeRoditelja,
                Prezime = k.Prezime,
                Jmbg = k.Jmbg,
                DatumRodjenja = k.DatumRodjenja,
                RadnoMesto = k.RadnoMesto,
                Funkcija = k.Funkcija,
                BrojKancelarije = k.BrojKancelarije,
                Telefon = k.Telefon,
                Email = k.Email
            };
            if (!NeUcitavajSifru)
            {
                Sifre sifra = k.Sifre.ToList().Find(x => x.Trenutna);
            
                if (sifra != null)
                    kr.TrenutnaSifra = sifra.Sifra;
            }
            return kr;
        }
        public static List<KorisnikDTO> KorisniciListDTOGenerator(List<Korisnik> kl, bool NeUcitavajSifru = false)
        {
            List<KorisnikDTO> korisnici = new List<KorisnikDTO>();
            foreach (Korisnik k in kl)
            {
                korisnici.Add(DTOGenerator.KorisnikDTOGenerator(k, NeUcitavajSifru));
            }
            return korisnici;
        }

        public static SifraDTO SifraDTOGenerator(Sifre s)
        {
            return new SifraDTO()
            {
                SifraId = s.SifraId,
                Sifra = s.Sifra,
                DatumPostavljanja = s.DatumPostavljanja,
                Trenutna = s.Trenutna,
                KorisnikId = s.Korisnik.KorisnikId
            };
        }
        public static List<SifraDTO> SifreListDTOGenerator(List<Sifre> sl)
        {
            List<SifraDTO> sifre = new List<SifraDTO>();
            foreach (Sifre s in sl)
            {
                sifre.Add(DTOGenerator.SifraDTOGenerator(s));
            }
            return sifre;
        }

        public static ProfilDTO ProfilDTOGenerator(Profil p)
        {
            ProfilDTO pd = new ProfilDTO()
            {
                BojaPozadine = p.BojaPozadine,
                KorisnikId = p.Korisnik.KorisnikId,
                RedniBroj = p.RedniBroj
            };
            pd.GuiElementi = GuiElementListDTOGEnerator(p.GuiElementi.ToList());
            return pd;
        }
        public static List<ProfilDTO> ProfilListDTOGenerator(List<Profil> pl)
        {
            List<ProfilDTO> pdl = new List<ProfilDTO>();
            foreach (Profil p in pl)
            {
                pdl.Add(DTOGenerator.ProfilDTOGenerator(p));
            }
            return pdl;
        }

        public static GuiElementDTO GuiElementDTOGenerator(GuiElement g)
        {
            return new GuiElementDTO()
            {
                ElementId = g.ElementId,
                Naziv = g.Naziv
            };
        }
        public static List<GuiElementDTO> GuiElementListDTOGEnerator(List<GuiElement> gl)
        {
            List<GuiElementDTO> gdl = new List<GuiElementDTO>();
            foreach (GuiElement guiElement in gl)
            {
                gdl.Add(DTOGenerator.GuiElementDTOGenerator(guiElement));
            }
            return gdl;
        }

        public static PrijavaDTO PrijavaDTOGenerator(Prijavljivanje p)
        {
            return new PrijavaDTO()
            {
                Sifra = p.Sifra,
                korisnikId = p.Korisnik.KorisnikId,
                IpAdresa = p.IpAdresa,
                PrijavaId = p.PrijavaId,
                Uspesna = p.Uspesna,
                Vreme = p.Vreme
            };
        }
        public static List<PrijavaDTO> PrijavaListDTOGenerator(List<Prijavljivanje> pl)
        {
            List<PrijavaDTO> list = new List<PrijavaDTO>(pl.Count);

            foreach (Prijavljivanje p in pl)
            {
                list.Add(DTOGenerator.PrijavaDTOGenerator(p));
            }
            return list;
        }

        public static PrivilegijaDTO PrivilegijaDTOGenerator(Privilegija p)
        {
            PrivilegijaDTO pd;
            p = (Privilegija)DataLayer.Sesija.GetSessionImplementation().PersistenceContext.Unproxy(p);
            //to-do: replace IFs
            if (p is AdminPrivilegija)
            {
                AdminPrivilegija pa = p as AdminPrivilegija;
                pd = new AdminPrivilegijaDTO()
                {
                    Naziv = pa.Naziv,
                    PrivilegijaId = pa.PrivilegijaId
                };
            }
            else if (p is FunkcionalnaPrivilegija)
            {
                FunkcionalnaPrivilegija pf = p as FunkcionalnaPrivilegija;
                pd = new FunckionalnaPrivilegijaDTO()
                {
                    Naziv = pf.Naziv,
                    PrivilegijaId = pf.PrivilegijaId
                };
            }
            else if (p is InterfejsPrivilegija)
            {
                InterfejsPrivilegija pi = p as InterfejsPrivilegija;
                pd = new InterfejsPrivilegijaDTO()
                {
                    Naziv = pi.Naziv,
                    PrivilegijaId = pi.PrivilegijaId,
                    DozvolaKoriscenja = pi.DozvolaKoriscenja
                };
            }
            else //if (p is MeniPrivilegija)
            {
                MeniPrivilegija pm = p as MeniPrivilegija;
                
                pd = new MeniPrivilegijaDTO()
                {
                    Naziv = pm.Naziv,
                    PrivilegijaId = pm.PrivilegijaId,
                    RoditeljskiMeniID = (pm.RoditeljskiMeni != null) ? pm.RoditeljskiMeni.PrivilegijaId : 0,
                    PodkontroleID = new List<int>()
                };
                
                foreach (MeniPrivilegija mpriv in pm.Podkontrole)
                {
                    (pd as MeniPrivilegijaDTO).PodkontroleID.Add(mpriv.PrivilegijaId);
                }
            }
            return pd;
        }
        public static List<PrivilegijaDTO> PrivilegijeListDTOGenerator(List<Privilegija> pl)
        {
            List<PrivilegijaDTO> rezultat = new List<PrivilegijaDTO>();
            foreach (Privilegija g in pl)
            {
                rezultat.Add(DTOGenerator.PrivilegijaDTOGenerator(g));
            }
            return rezultat;
        }

        public static GrupaDTO GrupaDtoGenerator(Grupa g)
        {
            GrupaDTO gd;

            if (g is RegularnaGrupa)
                gd = new RegularnaGrupaDTO();     
            else 
                gd = new KorisnickaGrupaDTO()
                {
                    KorisnikID = ((KorisnickaGrupa)g).Korisnik.KorisnikId
                };

            gd.Ime = g.Ime;
            gd.GrupaId = g.GrupaId;
            gd.Opis = g.Opis;
            gd.DatumKreiranja = g.DatumKreiranja;
            gd.LoginVremeDo = g.LoginVremeDo;
            gd.LoginVremeOd = g.LoginVremeDo;

            return gd;
        }
        public static List<GrupaDTO> GrupeDtoListaGenerator(List<Grupa> lista)
        {
            List<GrupaDTO> rezultat = new List<GrupaDTO>();
            foreach (Grupa g in lista)
            {
                rezultat.Add(DTOGenerator.GrupaDtoGenerator(g));
            }
            return rezultat;
        }
        public static List<RegularnaGrupaDTO> RGrupaListDTOGenerator(List<RegularnaGrupa> lista)
        {
            List<RegularnaGrupaDTO> rlist = new List<RegularnaGrupaDTO>(lista.Count);
            foreach (RegularnaGrupa g in lista)
            {
                rlist.Add((RegularnaGrupaDTO) DTOGenerator.GrupaDtoGenerator(g));
            }
            return rlist;
        }
        public static List<KorisnickaGrupaDTO> KGrupaListDTOGenerator(List<KorisnickaGrupa> lista)
        {
            List<KorisnickaGrupaDTO> rlist = new List<KorisnickaGrupaDTO>(lista.Count);
            foreach (KorisnickaGrupa g in lista)
            {
                rlist.Add((KorisnickaGrupaDTO) DTOGenerator.GrupaDtoGenerator(g));
            }
            return rlist;
        }

        public static IPDTO IpDtoGenerator(Ip ip)
        {
            return new IPDTO()
            {
                IpAdresa = ip.IpAdresa
            };
        }
        public static List<IPDTO> IpDtoListaGenerator(List<Ip> lista)
        {
            List<IPDTO> rezultat = new List<IPDTO>();
            foreach (Ip t in lista)
            {
                rezultat.Add(DTOGenerator.IpDtoGenerator(t));
            }
            return rezultat;
        }

        public static Prijavljivanje PrijavaIzDto(PrijavaDTO p)
        {
            ISession ses = DataLayer.OtvoriSesiju();
            return new Prijavljivanje()
            {
                IpAdresa = p.IpAdresa,
                Sifra = p.Sifra,
                Korisnik = ses.Load<Korisnik>(p.korisnikId),
                Uspesna = p.Uspesna,
                Vreme = DateTime.Now
            };
        }

        public static FunkcionalnaPrivilegija FunkcionalnaIzDto(FunckionalnaPrivilegijaDTO f)
        {
            return new FunkcionalnaPrivilegija()
            {
                Naziv = f.Naziv
            };
        }

        public static MeniPrivilegija MeniIzDto(MeniPrivilegijaDTO f)
        {
            return new MeniPrivilegija()
            {
                Naziv = f.Naziv,
            };
        }
        public static AdminPrivilegija AdminIzDto(AdminPrivilegijaDTO f)
        {
            return new AdminPrivilegija()
            {
                Naziv = f.Naziv,
            };
        }

        public static InterfejsPrivilegija InterfejsIzDto(InterfejsPrivilegijaDTO f)
        {
            return new InterfejsPrivilegija()
            {
                Naziv = f.Naziv,
            };
        }

        public static AdminPrivilegijaDTO DtoAdmin(AdminPrivilegija f)
        {
            return new AdminPrivilegijaDTO()
            {
                Naziv = f.Naziv,
                PrivilegijaId = f.PrivilegijaId
            };
        }

        public static InterfejsPrivilegijaDTO DtoInterfejs(InterfejsPrivilegija f)
        {
            return new InterfejsPrivilegijaDTO()
            {
                Naziv = f.Naziv,
                PrivilegijaId = f.PrivilegijaId
            };
        }

        public static MeniPrivilegijaDTO DtoMeni(MeniPrivilegija f)
        {
            return new MeniPrivilegijaDTO()
            {
                Naziv = f.Naziv,
                PrivilegijaId = f.PrivilegijaId
            };
        }

        public static FunckionalnaPrivilegijaDTO DtoFunckionalna(FunkcionalnaPrivilegija f)
        {
            return new FunckionalnaPrivilegijaDTO()
            {
                Naziv = f.Naziv,
                PrivilegijaId = f.PrivilegijaId
            };
        }

        public static Sifre SifraIzDto(SifraDTO s)
        {
            ISession ses = DataLayer.OtvoriSesiju();
            return new Sifre()
            {
                Korisnik = ses.Load<Korisnik>(s.KorisnikId),
                Sifra = s.Sifra,
                DatumPostavljanja = DateTime.Now,
                Trenutna = true
            };
        }

        public static KorisnickaGrupaDTO KGrupaDto(KorisnickaGrupa k)
        {
            return new KorisnickaGrupaDTO()
            {
                Ime = k.Ime,
                GrupaId = k.GrupaId,
                DatumKreiranja = k.DatumKreiranja,
                LoginVremeDo = k.LoginVremeDo,
                LoginVremeOd = k.LoginVremeDo,
                KorisnikID = k.Korisnik.KorisnikId
            };
        }

        public static RegularnaGrupaDTO RGrupaDto(RegularnaGrupa k)
        {
            return new RegularnaGrupaDTO()
            {
                Ime = k.Ime,
                GrupaId = k.GrupaId,
                DatumKreiranja = k.DatumKreiranja,
                LoginVremeDo = k.LoginVremeDo,
                LoginVremeOd = k.LoginVremeDo
            };
        }
    }
}
