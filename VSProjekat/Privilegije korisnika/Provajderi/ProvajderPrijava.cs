﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using Privilegije_korisnika.Provajderi.DTO;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Provajderi
{
    public class ProvajderPrijava
    {
        public static List<PrijavaDTO> SvePrijave()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<Prijavljivanje> prijave = ses.Query<Prijavljivanje>().Select(x => x).ToList();
                return DTOGenerator.PrijavaListDTOGenerator(prijave);

            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static PrijavaDTO VratiPrijavu(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Prijavljivanje p = ses.Get<Prijavljivanje>(id);
                return DTOGenerator.PrijavaDTOGenerator(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiPrijavu(PrijavaDTO pdto)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Prijavljivanje p = ses.Get<Prijavljivanje>(pdto.PrijavaId);
                ses.Delete(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajPrijavu(PrijavaDTO pdto)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Prijavljivanje p = ses.Get<Prijavljivanje>(pdto.PrijavaId);
                p.IpAdresa = pdto.IpAdresa != string.Empty ? pdto.IpAdresa : p.IpAdresa;
                p.Korisnik = pdto.korisnikId > 0 ? ses.Get<Korisnik>(pdto.korisnikId) : p.Korisnik;
                p.Sifra = pdto.Sifra != String.Empty ? pdto.Sifra : p.Sifra;
                p.Uspesna = pdto.Uspesna;
                p.Vreme = pdto.Vreme;
                ses.Update(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajPrijavu(PrijavaDTO pdto)
        {
            try
            {
                Prijavljivanje p = DTOGenerator.PrijavaIzDto(pdto);

                ISession ses = DataLayer.OtvoriSesiju();

                ses.Save(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static List<SifraDTO> SveSifre()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<Sifre> sifre = ses.Query<Sifre>().Select(x => x).ToList();
                return DTOGenerator.SifreListDTOGenerator(sifre);

            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static SifraDTO VratiSifru(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Sifre p = ses.Get<Sifre>(id);
                return DTOGenerator.SifraDTOGenerator(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiSifru(SifraDTO s)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Sifre p = ses.Get<Sifre>(s.SifraId);
                ses.Delete(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajSifru(SifraDTO s)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Sifre sifra = ses.Get<Sifre>(s.SifraId);
                sifra.DatumPostavljanja = DateTime.Now;
                sifra.Sifra = s.Sifra != String.Empty ? s.Sifra : sifra.Sifra;
                sifra.Trenutna = true;
                ses.Update(sifra);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajSifru(SifraDTO s)
        {
            try
            {
                Sifre sifra = DTOGenerator.SifraIzDto(s);

                ISession ses = DataLayer.OtvoriSesiju();

                ses.Save(sifra);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
    }
}
