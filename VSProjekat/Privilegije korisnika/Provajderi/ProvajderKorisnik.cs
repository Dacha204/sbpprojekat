﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Provajderi
{
    public static class ProvajderKorisnik
    {
        public static KorisnikDTO VratiKorisnika(int kid)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();

                Korisnik k = ses.Get<Korisnik>(kid);
                if (k == null)
                    return null;

                return DTOGenerator.KorisnikDTOGenerator(k);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<KorisnikDTO> VratiSveKorisnike(bool NeUcitavajSifru = false)
        {
            try
            {
                ISession session = DataLayer.OtvoriSesiju();
                List<Korisnik> korisnici = session.Query<Korisnik>()
                    .Select(x => x)
                    .ToList();
                return DTOGenerator.KorisniciListDTOGenerator(korisnici, NeUcitavajSifru);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
        }

        public static SifraDTO VratiKorisnickuSifru(int kid)
        {
            try
            {
                ISession session = DataLayer.OtvoriSesiju();
                Korisnik k = session.Get<Korisnik>(kid);

                Sifre trenutna = k.Sifre.ToList().First(x => x.Trenutna);

                return DTOGenerator.SifraDTOGenerator(trenutna);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<SifraDTO> VratiKorisnikoveSifre(int kid)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Get<Korisnik>(kid);
                //korisnik pronadjen validacija
                return DTOGenerator.SifreListDTOGenerator(k.Sifre.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static void AzurirajKorisnika(KorisnikDTO kd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Get<Korisnik>(kd.KorisnikId);
                k.KorisnickoIme = kd.KorisnickoIme;
                k.Ime = kd.Ime;
                k.ImeRoditelja = kd.ImeRoditelja;
                k.Prezime = kd.Prezime;
                k.Jmbg = kd.Jmbg;
                k.DatumRodjenja = kd.DatumRodjenja;
                k.RadnoMesto = kd.RadnoMesto;
                k.Funkcija = kd.Funkcija;
                k.BrojKancelarije = kd.BrojKancelarije;
                k.Telefon = kd.Telefon;
                k.Email = kd.Email;
                
                if (kd.TrenutnaSifra != k.Sifre.First(x => x.Trenutna).Sifra)
                    AzurirajLozinkuKorisnika(k, kd.TrenutnaSifra, ses);
                
                ses.SaveOrUpdate(k);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static int KreirajKorisnika(KorisnikDTO kd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();

                Korisnik k = new Korisnik();
                k.KorisnickoIme = kd.KorisnickoIme;
                k.Ime = kd.Ime;
                k.ImeRoditelja = kd.ImeRoditelja;
                k.Prezime = kd.Prezime;
                k.Jmbg = kd.Jmbg;
                k.DatumRodjenja = kd.DatumRodjenja;
                k.RadnoMesto = kd.RadnoMesto;
                k.Funkcija = kd.Funkcija;
                k.BrojKancelarije = kd.BrojKancelarije;
                k.Telefon = kd.Telefon;
                k.Email = kd.Email;
                AzurirajLozinkuKorisnika(k, kd.TrenutnaSifra, ses);
                
                KorisnickaGrupa kg = new KorisnickaGrupa();
                kg.DatumKreiranja = DateTime.Now;
                kg.Ime = k.KorisnickoIme;
                kg.Opis = "Korisnik:"+k.KorisnickoIme;
                kg.Korisnik = k;

                k.KorisnickaGrupa = kg;
                
                ses.SaveOrUpdate(k);
                ses.SaveOrUpdate(kg);
                ses.Flush();
                return k.KorisnikId;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return 0;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        
        public static void ObrisiKorisnika(KorisnikDTO kd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kd.KorisnikId);
                ses.Delete(k);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziGresku("Brisanje nije moguce (Contraint)\n" + e.Message);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        private static void AzurirajLozinkuKorisnika(Korisnik k, string novaSifra, ISession s)
        {
            Sifre Sifre = new Sifre()
            {
                Korisnik = k,
                Trenutna = true,
                Sifra = novaSifra,
                DatumPostavljanja = DateTime.Now
            };

            k.Sifre.ToList().ForEach(x => x.Trenutna = false);
            k.Sifre.Add(Sifre);
            //s.SaveOrUpdate(Sifre);
        }

        public static List<ProfilDTO> VratiKorisnikoveProfile(int kid)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kid);

                return DTOGenerator.ProfilListDTOGenerator(k.Profili.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajProfil(ProfilDTO pd, List<GuiElementDTO> guis)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(pd.KorisnikId);

                Profil p = k.Profili.First(x => x.RedniBroj == pd.RedniBroj);
                p.BojaPozadine = pd.BojaPozadine;
                
                ses.SaveOrUpdate(p);
                AzurirajGUIElementeProfila(pd, guis);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void KreirajProfil(ProfilDTO pd, List<GuiElementDTO> guis)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(pd.KorisnikId);

                Profil p = new Profil()
                {
                    Korisnik = k,
                    BojaPozadine = pd.BojaPozadine
                };
                
                ses.SaveOrUpdate(p);
                pd.RedniBroj = p.RedniBroj;
                AzurirajGUIElementeProfila(pd, guis);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }


        public static void ObrisiProfil(ProfilDTO pd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Profil p = ses.Load<Profil>(pd.RedniBroj);
                ses.Delete(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziGresku("Brisanje nije moguce (Contraint)\n" + e.Message);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void PrifilUpdate(ProfilDTO pd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Profil p = ses.Load<Profil>(pd.RedniBroj);
                p.BojaPozadine = pd.BojaPozadine;
                p.Korisnik = ses.Load<Korisnik>(pd.KorisnikId);
                ses.Update(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziGresku("Azuriranje nije moguce (Contraint)\n" + e.Message);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static ProfilDTO VratiProfil(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Profil p = ses.Load<Profil>(id);
                return DTOGenerator.ProfilDTOGenerator(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziGresku("Azuriranje nije moguce (Contraint)\n" + e.Message);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajProfil(ProfilDTO pd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Profil p = new Profil();
                p.BojaPozadine = pd.BojaPozadine;
                p.Korisnik = ses.Load<Korisnik>(pd.KorisnikId);
                ses.Save(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziGresku("Azuriranje nije moguce (Contraint)\n" + e.Message);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<ProfilDTO> SviProfili()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<Profil> lista = ses.Query<Profil>().Select(x => x).ToList();
                return DTOGenerator.ProfilListDTOGenerator(lista);
            } 
            catch (Exception e)
            {
                Utility.PrikaziGresku("Azuriranje nije moguce (Contraint)\n" + e.Message);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
             
        public static List<GuiElementDTO> VratiGUIElemente()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<GuiElement> elementi = ses.Query<GuiElement>().Select(x => x)
                    .ToList();
                return DTOGenerator.GuiElementListDTOGEnerator(elementi);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static GuiElementDTO VratiGUElement(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                GuiElement ge = ses.Load<GuiElement>(id);
                return DTOGenerator.GuiElementDTOGenerator(ge);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void KreirajGUIElement(GuiElementDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                GuiElement g = new GuiElement()
                {
                    Naziv = gd.Naziv
                };
                ses.SaveOrUpdate(g);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajGUIElement(GuiElementDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                GuiElement g = ses.Load<GuiElement>(gd.ElementId);

                g.Naziv = gd.Naziv;
                ses.SaveOrUpdate(g);
                ses.Flush();
                
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiGUIElement(GuiElementDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                GuiElement g = ses.Load<GuiElement>(gd.ElementId);
                ses.Delete(g);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziGresku("Brisanje nije moguce (Contraint)\n" + e.Message);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }


        public static List<GuiElementDTO> VratiGUIElementeProfila(ProfilDTO pd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Profil p = ses.Load<Profil>(pd.RedniBroj);
                return DTOGenerator.GuiElementListDTOGEnerator(p.GuiElementi.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajGUIElementeProfila(ProfilDTO pd, List<GuiElementDTO> guis)
        {
            try
            {
                ISession ses = DataLayer.Sesija;
                Profil p = ses.Load<Profil>(pd.RedniBroj);

                IList<GuiElement> novalista = new List<GuiElement>(guis.Count);
                foreach (GuiElementDTO gd in guis)
                {
                    novalista.Add(ses.Load<GuiElement>(gd.ElementId));
                }

                p.GuiElementi = novalista;
                
                ses.SaveOrUpdate(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static List<PrijavaDTO> VratiPrijaveKorisnika(KorisnikDTO kd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kd.KorisnikId);
                return DTOGenerator.PrijavaListDTOGenerator(k.Prijave.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool PrijaviKorisnika(string username, string password, string ip)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                
                Korisnik k = ses.QueryOver<Korisnik>()
                    .Where(x => x.KorisnickoIme == username)
                    .SingleOrDefault();

                if (k == null)
                {
                    Utility.PrikaziGresku("Korisnicko ime nevalidno!");
                    return false;
                }

                Prijavljivanje pr = new Prijavljivanje()
                {
                    Korisnik = k,
                    Sifra = password,
                    IpAdresa = ip,
                    Vreme = DateTime.Now
                };

                //validacija sifre
                if (k.Sifre.ToList().Find(x => x.Trenutna).Sifra != password)
                {
                    Utility.PrikaziGresku("Korisnicka lozinka je pogresna");
                    pr.Uspesna = false;
                }
                //validacija login vremena
                else if (!PrivilegijaLoginVreme(k))
                {
                    Utility.PrikaziGresku("Prijava u nedozvoljeno vreme");
                    pr.Uspesna = false;
                }
                else if (!PrivilegijaLoginIP(k))
                {
                    Utility.PrikaziGresku("Prijavljivanje sa nedozvoljene IP adrese");
                    pr.Uspesna = false;
                }
                else
                {
                    pr.Uspesna = true;
                }

                ses.SaveOrUpdate(pr);
                ses.Flush();
                return pr.Uspesna;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        #region IspitivanjePrivilegija
        private static bool PrivilegijaLoginVreme(Korisnik k)
        {
            return true;
        }
        private static bool PrivilegijaLoginIP(Korisnik k)
        {
            return true;
        }
        #endregion

    }
}
