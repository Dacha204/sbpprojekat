﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class ProfilDTO
    {
        public int RedniBroj { get; set; }
        public string BojaPozadine { get; set; }
        public int KorisnikId { get; set; }
        public List<GuiElementDTO> GuiElementi { get; set; }

        public ProfilDTO()
        {
            GuiElementi = new List<GuiElementDTO>();
        }
    }
}
