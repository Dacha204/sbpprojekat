﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class MeniPrivilegijaDTO : PrivilegijaDTO
    {
        public int RoditeljskiMeniID { get; set; }
        public List<int> PodkontroleID { get; set; }
    }
}
