﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class InterfejsPrivilegijaDTO : PrivilegijaDTO
    {
        public bool DozvolaKoriscenja { get; set; }
    }
}
