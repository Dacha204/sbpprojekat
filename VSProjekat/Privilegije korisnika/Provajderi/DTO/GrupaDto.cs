﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class GrupaDTO
    {
        public int GrupaId { get; set; }
        public string Ime { get; set; }
        public string Opis { get; set; }
        public DateTime DatumKreiranja { get; set; }
        public DateTime? LoginVremeOd { get; set; }
        public DateTime? LoginVremeDo { get; set; }

        public  List<PrivilegijaDTO> Privilegije { get; set; }
        public virtual List<IPDTO> IpAdrese { get; set; }

        public GrupaDTO()
        {
            IpAdrese = new List<IPDTO>();
            Privilegije = new List<PrivilegijaDTO>();
        }
    }
}
