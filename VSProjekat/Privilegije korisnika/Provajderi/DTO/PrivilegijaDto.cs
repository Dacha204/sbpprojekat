﻿using System;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public abstract class PrivilegijaDTO
    {
        public int PrivilegijaId { get; set; }
        public string Naziv { get; set; }     
    }
}