﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class KorisnikDTO
    {
        public int KorisnikId { get; set; }
        public string KorisnickoIme { get; set; }
        public string Ime { get; set; }
        public string ImeRoditelja { get; set; }
        public string Prezime { get; set; }
        public string Jmbg { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public string RadnoMesto { get; set; }
        public string Funkcija { get; set; }
        public string BrojKancelarije { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string TrenutnaSifra { get; set; }

        public string PunoIme => $"{Ime} {Prezime}";
    }
}
