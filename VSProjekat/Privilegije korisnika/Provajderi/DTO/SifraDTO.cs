﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class SifraDTO
    {
        public int SifraId { get; set; }
        public string Sifra { get; set; }
        public DateTime DatumPostavljanja { get; set; }
        public bool Trenutna { get; set; }
        public int KorisnikId { get; set; }
    }
}
