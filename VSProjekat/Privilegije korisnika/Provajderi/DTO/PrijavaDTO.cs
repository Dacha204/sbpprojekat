﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class PrijavaDTO
    {
        public int PrijavaId { get; set; }
        public bool Uspesna { get; set; }
        public string IpAdresa { get; set; }
        public DateTime Vreme { get; set; }
        public string Sifra { get; set; }

        public int korisnikId { get; set; }
    }
}
