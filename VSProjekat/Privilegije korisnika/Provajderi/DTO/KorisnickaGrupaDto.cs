﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Provajderi.DTO
{
    public class KorisnickaGrupaDTO : GrupaDTO
    {
        public int KorisnikID { get; set; }
    }
}
