﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using Privilegije_korisnika.Entiteti;
using Privilegije_korisnika.Provajderi.DTO;

namespace Privilegije_korisnika.Provajderi
{
    public class ProvajderGrupa
    {
        public static void Template()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                //work

            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static List<GrupaDTO> SveGrupe()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<Grupa> lista = ses.Query<Grupa>().Select(x => x).ToList();
                return DTOGenerator.GrupeDtoListaGenerator(lista);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<KorisnickaGrupaDTO> SveKorisnickeGrupe()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<KorisnickaGrupa> lista = ses.Query<KorisnickaGrupa>().Select(x => x).ToList();
                //lista.RemoveAll(x => x is RegularnaGrupa);
                return DTOGenerator.KGrupaListDTOGenerator(lista);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<RegularnaGrupaDTO> SveRegularneGrupe()
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                List<RegularnaGrupa> lista = ses.Query<RegularnaGrupa>().Select(x => x).ToList();
               return DTOGenerator.RGrupaListDTOGenerator(lista);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static List<RegularnaGrupaDTO> RGrupeUGrupi(RegularnaGrupaDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa g = ses.Load<RegularnaGrupa>(gd.GrupaId);
                return DTOGenerator.RGrupaListDTOGenerator(g.SadrziGrupe.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static List<KorisnickaGrupaDTO> KGrupeUGrupi(RegularnaGrupaDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa g = ses.Load<RegularnaGrupa>(gd.GrupaId);
                return DTOGenerator.KGrupaListDTOGenerator(g.SadrziKorisnike.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static List<RegularnaGrupaDTO> VratiGrupeKorisnika(KorisnikDTO kd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                Korisnik k = ses.Load<Korisnik>(kd.KorisnikId);
                return DTOGenerator.RGrupaListDTOGenerator(k.KorisnickaGrupa.PripadaGrupama.ToList());
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }

        }
        public static bool DodajGrupuUGrupu(RegularnaGrupaDTO parrent, RegularnaGrupaDTO child)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa roditelj = ses.Load<RegularnaGrupa>(parrent.GrupaId);
                RegularnaGrupa dete = ses.Load<RegularnaGrupa>(child.GrupaId);

                roditelj.DodajPodgrupu(dete);
                ses.SaveOrUpdate(roditelj);
                ses.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool DodajKorisnikaUGrupu(RegularnaGrupaDTO parrent, KorisnickaGrupaDTO child)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa roditelj = ses.Load<RegularnaGrupa>(parrent.GrupaId);
                Korisnik korisnik = ses.Load<Korisnik>(child.KorisnikID);
                roditelj.DodajKorisnika(korisnik.KorisnickaGrupa);
                ses.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }

        }

        public static bool UkloniGrupuIzGrupe(RegularnaGrupaDTO parrent, RegularnaGrupaDTO child)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa roditelj = ses.Load<RegularnaGrupa>(parrent.GrupaId);
                RegularnaGrupa dete = ses.Load<RegularnaGrupa>(child.GrupaId);

                roditelj.UkoniPodgrupu(dete);
                ses.SaveOrUpdate(roditelj);
                ses.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();

            }
        }
        public static bool UkloniKorisnikaIzGrupu(RegularnaGrupaDTO parrent, KorisnickaGrupaDTO child)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa roditelj = ses.Load<RegularnaGrupa>(parrent.GrupaId);
                Korisnik korisnik = ses.Load<Korisnik>(child.KorisnikID);

                roditelj.UkloniKorisnika(korisnik.KorisnickaGrupa);
                ses.SaveOrUpdate(roditelj);
                ses.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static bool NapraviGrupu(RegularnaGrupaDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa rg = new RegularnaGrupa()
                {
                    DatumKreiranja = DateTime.Now,
                    Ime = gd.Ime,
                    Opis = gd.Opis,
                    LoginVremeOd = gd.LoginVremeOd,
                    LoginVremeDo = gd.LoginVremeDo
                };

                ses.SaveOrUpdate(rg);
                ses.Flush();

                ProvajderPrivilegija.AzurirajIPGrupe(rg, gd.IpAdrese);
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool IzmeniGrupu(RegularnaGrupaDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa rg = ses.Load<RegularnaGrupa>(gd.GrupaId);
                rg.Ime = gd.Ime;
                rg.Opis = gd.Opis;
                rg.LoginVremeOd = gd.LoginVremeOd;
                rg.LoginVremeDo = gd.LoginVremeDo;

                ses.SaveOrUpdate(rg);
                ses.Flush();

                ProvajderPrivilegija.AzurirajIPGrupe(rg, gd.IpAdrese);
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static bool ObrisiGrupu(RegularnaGrupaDTO gd)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa rg = ses.Load<RegularnaGrupa>(gd.GrupaId);

                ses.Delete(rg);
                ses.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Utility.PrikaziIzuetak(ex);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static bool UcitajIPGrupe(RegularnaGrupaDTO dt)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa r = ses.Load<RegularnaGrupa>(dt.GrupaId);
                dt.IpAdrese = DTOGenerator.IpDtoListaGenerator(r.IpAdrese.ToList());
                return true;
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return false;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static KorisnickaGrupaDTO VratiKorisnickuGrupu(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                KorisnickaGrupa p = ses.Get<KorisnickaGrupa>(id);
                return DTOGenerator.KGrupaDto(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiKorisnickuGrupu(KorisnickaGrupaDTO k)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                KorisnickaGrupa p = ses.Get<KorisnickaGrupa>(k.GrupaId);
                Korisnik ko = ses.Get<Korisnik>(k.KorisnikID);
                ses.Delete(ko);
                ses.Delete(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajKorisnickuGrupu(KorisnickaGrupaDTO k)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                KorisnickaGrupa p = new KorisnickaGrupa();
                p.DatumKreiranja = DateTime.Now;
                p.Ime = k.Ime;
                p.LoginVremeDo = k.LoginVremeDo;
                p.LoginVremeOd = k.LoginVremeOd;
                p.Korisnik = ses.Load<Korisnik>(k.KorisnikID);
                ses.Save(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajKorisnickuGrupu(KorisnickaGrupaDTO k)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                KorisnickaGrupa p = ses.Load<KorisnickaGrupa>(k.GrupaId);
                p.DatumKreiranja = DateTime.Now;
                p.Ime = k.Ime;
                p.LoginVremeDo = k.LoginVremeDo;
                p.LoginVremeOd = k.LoginVremeOd;
                ses.Update(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }

        public static RegularnaGrupaDTO VratiRegularnuGrupu(int id)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa p = ses.Get<RegularnaGrupa>(id);
                return DTOGenerator.RGrupaDto(p);
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
                return null;
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void ObrisiRegularnuGrupu(RegularnaGrupaDTO k)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa p = ses.Get<RegularnaGrupa>(k.GrupaId);
                ses.Delete(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void DodajRegularnuGrupu(RegularnaGrupaDTO k)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa p = new RegularnaGrupa();
                p.DatumKreiranja = DateTime.Now;
                p.Ime = k.Ime;
                p.LoginVremeDo = k.LoginVremeDo;
                p.LoginVremeOd = k.LoginVremeOd;
                p.Opis = k.Opis;
                ses.Save(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
        public static void AzurirajRegularnuGrupu(RegularnaGrupaDTO k)
        {
            try
            {
                ISession ses = DataLayer.OtvoriSesiju();
                RegularnaGrupa p = ses.Load<RegularnaGrupa>(k.GrupaId);
                p.DatumKreiranja = DateTime.Now;
                p.Ime = k.Ime;
                p.LoginVremeDo = k.LoginVremeDo;
                p.LoginVremeOd = k.LoginVremeOd;
                ses.Update(p);
                ses.Flush();
            }
            catch (Exception e)
            {
                Utility.PrikaziIzuetak(e);
            }
            finally
            {
                DataLayer.ZatvoriSesiju();
            }
        }
    }
}