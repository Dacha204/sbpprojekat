﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class RegularnaGrupa : Grupa
    {
        
        protected virtual IList<Grupa> Sadrzi { get; set; } 

        public virtual IList<RegularnaGrupa> PripadaGrupama => Sadrzi.OfType<RegularnaGrupa>().ToList();
        public virtual IList<KorisnickaGrupa> SadrziKorisnike => Sadrzi.OfType<KorisnickaGrupa>().ToList();
        public virtual IList<RegularnaGrupa> SadrziGrupe => Sadrzi.OfType<RegularnaGrupa>().ToList();

        //experimental

        public virtual void DodajPodgrupu(RegularnaGrupa dete)
        {
            Sadrzi.Add(dete);
        }
        public virtual void DodajKorisnika(KorisnickaGrupa korisnik)
        {
            Sadrzi.Add(korisnik);
        }

        public virtual void UkoniPodgrupu(RegularnaGrupa dete)
        {
            Sadrzi.Remove(dete);
        }
        public virtual void UkloniKorisnika(KorisnickaGrupa korisnik)
        {
            Sadrzi.Remove(korisnik);
        }

        public RegularnaGrupa()
        {
            Sadrzi = new List<Grupa>();
        }
    }
}
