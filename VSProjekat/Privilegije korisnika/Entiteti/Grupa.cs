﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
     public abstract class Grupa
    {
        public virtual int GrupaId { get; set; }
        public virtual string Ime { get; set; }
        public virtual string Opis { get; set; }
        public virtual DateTime DatumKreiranja { get; set; } 
        public virtual DateTime? LoginVremeOd { get; set; } 
        public virtual DateTime? LoginVremeDo { get; set; } 

        public virtual IList<DodelaPrivilegijeVeza> ImaDodelePrivilegija { get; set; }
        public virtual IList<Ip> IpAdrese { get; set; }

        public Grupa()
        {
            IpAdrese = new List<Ip>();
            ImaDodelePrivilegija = new List<DodelaPrivilegijeVeza>();
        }
    }
}
