﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.ComponentModel.Com2Interop;

namespace Privilegije_korisnika.Entiteti
{
    public class Sifre
    {
        public virtual int SifraId { get; set; }
        public virtual string Sifra { get; set; }
        public virtual DateTime DatumPostavljanja { get; set; } 
        public virtual bool Trenutna { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
