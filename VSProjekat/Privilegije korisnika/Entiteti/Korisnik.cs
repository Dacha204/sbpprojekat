﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class Korisnik
    {
        public virtual int KorisnikId { get; set; }
        public virtual string KorisnickoIme { get; set; }
        public virtual string Ime { get; set; }
        public virtual string ImeRoditelja { get; set; }
        public virtual string Prezime { get; set; }
        public virtual string Jmbg { get; set; }
        public virtual DateTime DatumRodjenja { get; set; }
        public virtual string RadnoMesto { get; set; }
        public virtual string Funkcija { get; set; }
        public virtual string BrojKancelarije { get; set; }
        public virtual string Telefon { get; set; }
        public virtual string Email { get; set; }

        public virtual KorisnickaGrupa KorisnickaGrupa { get; set; }

        public virtual IList<Prijavljivanje> Prijave { get; set; }
        public virtual IList<Sifre> Sifre { get; set; }
        public virtual IList<Profil> Profili { get; set; }
        public virtual IList<DodelaPrivilegijeVeza> DodelioPrivilegije { get; set; }

        public Korisnik()
        {
           Prijave = new List<Prijavljivanje>();
           Sifre = new List<Sifre>();
           Profili = new List<Profil>();
           DodelioPrivilegije = new List<DodelaPrivilegijeVeza>();
        }
    }
}
