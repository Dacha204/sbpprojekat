﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class Ip
    {
        public virtual string IpAdresa { get; set; }

        public virtual IList<Grupa> Grupe { get; set; }

        public Ip()
        {
            Grupe = new List<Grupa>();
        }
       
    }
}
