﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class GuiElement
    {
        public virtual int ElementId { get; set; }
        public virtual string Naziv { get; set; }

        public virtual IList<Profil> Profili { get; set; }

        public GuiElement()
        {
           Profili = new List<Profil>();
        }
    }
}
