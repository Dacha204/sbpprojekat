﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class Profil
    {
        public virtual int RedniBroj { get; set; }
        public virtual string BojaPozadine { get; set; }

        public virtual Korisnik Korisnik { get; set; }
        public virtual IList<GuiElement> GuiElementi { get; set; }

        public Profil()
        {
                GuiElementi = new List<GuiElement>();
        }
    }
}
