﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class DodelaPrivilegijeID
    {
        public virtual Grupa GrupaID { get; set; }
        public virtual Privilegija PrivilegijaID { get; set; }

        public override bool Equals(object obj)
        {
            if (Object.ReferenceEquals(this, obj))
                return true;

            if (obj.GetType() != typeof(DodelaPrivilegijeID))
                return false;

            DodelaPrivilegijeID receivedObject = (DodelaPrivilegijeID)obj;

            if ((GrupaID.GrupaId == receivedObject.GrupaID.GrupaId) &&
                (PrivilegijaID.PrivilegijaId == receivedObject.PrivilegijaID.PrivilegijaId))
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
