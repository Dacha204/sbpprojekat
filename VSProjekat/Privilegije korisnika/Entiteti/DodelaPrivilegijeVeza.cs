﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class DodelaPrivilegijeVeza
    {
        public virtual DodelaPrivilegijeID DodelaID { get; set; }
        public virtual Korisnik DodelioKorisnik { get; set; }
        public virtual DateTime DatumDodele { get; set; }

        public DodelaPrivilegijeVeza()
        {
            DodelaID = new DodelaPrivilegijeID();
        }
    }
}
