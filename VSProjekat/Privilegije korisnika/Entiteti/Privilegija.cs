﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public abstract class Privilegija
    {
        public virtual int PrivilegijaId { get; set; }
        public virtual string Naziv { get; set; }
        
        public virtual IList<DodelaPrivilegijeVeza> ImaDodeleGrupama { get; set; }

        public Privilegija()
        {
            ImaDodeleGrupama = new List<DodelaPrivilegijeVeza>();
        }
    }
}
