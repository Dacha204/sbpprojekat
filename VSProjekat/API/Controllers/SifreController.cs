﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;
using Privilegije_korisnika.Provajderi;

namespace API.Controllers
{
    [RoutePrefix("api/sifre")]
    public class SifreController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<SifraDTO> SveSifre()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<SifraDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderPrijava.SveSifre();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("vrati/{id:int}")]
        public SifraDTO VratiSifru(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            SifraDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderPrijava.VratiSifru(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpPut]
        [Route("azuriraj")]
        public void AzurirajSifru([FromBody] SifraDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrijava.AzurirajSifru(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi")]
        public void ObrisiSifru([FromBody] SifraDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrijava.ObrisiSifru(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj")]
        public void DodajSifru([FromBody] SifraDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrijava.DodajSifru(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }
    }
}
