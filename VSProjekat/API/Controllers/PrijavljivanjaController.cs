﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;

namespace API.Controllers
{
    [RoutePrefix("api/prijavljivanja")]
    public class PrijavljivanjaController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<PrijavaDTO> SvePrijave()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<PrijavaDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderPrijava.SvePrijave();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("vrati/{id:int}")]
        public PrijavaDTO VratiPrijavu(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            PrijavaDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderPrijava.VratiPrijavu(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpPut]
        [Route("azuriraj")]
        public void AzurirajPrijavu([FromBody] PrijavaDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrijava.AzurirajPrijavu(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi")]
        public void ObrisiPrijavu([FromBody] PrijavaDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrijava.ObrisiPrijavu(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj")]
        public void DodajPrijavu([FromBody] PrijavaDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrijava.DodajPrijavu(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }
    }
}
