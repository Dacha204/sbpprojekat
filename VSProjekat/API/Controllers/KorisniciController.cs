﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;

namespace API.Controllers
{
    [RoutePrefix("api/korisnici")]
    public class KorisniciController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<KorisnikDTO> SviKorisnici()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<KorisnikDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderKorisnik.VratiSveKorisnike();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("vrati/{id:int}")]
        public KorisnikDTO VratiKorisnika(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            KorisnikDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderKorisnik.VratiKorisnika(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpDelete]
        [Route("obrisi/{id:int}")]
        public void ObrisiKorisnika(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            KorisnikDTO k = Privilegije_korisnika.Provajderi.ProvajderKorisnik.VratiKorisnika(id);
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.ObrisiKorisnika(k);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("kreiraj")]
        public void KreirajKorisnika([FromBody] KorisnikDTO kdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.KreirajKorisnika(kdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPut]
        [Route("azuriraj")]
        public void AzurirajKorisnika([FromBody] KorisnikDTO kdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.AzurirajKorisnika(kdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }
    }
}
