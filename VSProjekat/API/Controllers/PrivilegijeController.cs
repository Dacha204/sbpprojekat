﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;
using Privilegije_korisnika.Provajderi;


namespace API.Controllers
{
    [RoutePrefix("api/privilegije")]
    public class PrivilegijeController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<PrivilegijaDTO> SvePrivilegije()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<PrivilegijaDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.SvePrivilegije();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("vrati/admin/{id:int}")]
        public PrivilegijaDTO VratiAdminPrivilegiju(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            PrivilegijaDTO p = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.VratiAdminPrivilegiju(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return p;
        }

        [HttpGet]
        [Route("vrati/interfejs/{id:int}")]
        public PrivilegijaDTO VratiInterfejsPrivilegiju(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            PrivilegijaDTO p = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.VratiInterfejsPrivilegiju(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return p;
        }

        [HttpGet]
        [Route("vrati/meni/{id:int}")]
        public PrivilegijaDTO VratiMeniPrivilegiju(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            PrivilegijaDTO p = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.VratiMeniPrivilegiju(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return p;
        }

        [HttpGet]
        [Route("vrati/funkcionalna/{id:int}")]
        public PrivilegijaDTO VrataFunckionalnuPrivilegiju(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            PrivilegijaDTO p = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.VratiFunckionalnuPrivilegiju(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return p;
        }

        [HttpPost]
        [Route("dodaj/funkcionalna")]
        public void DodajFunkcionalnuPrivilegiju([FromBody]FunckionalnaPrivilegijaDTO fpdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.
              DodajFunkcionalnuPrivilegiju(fpdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj/meni")]
        public void DodajMeniPrivilegiju([FromBody]MeniPrivilegijaDTO mpdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.
                DodajMeniPrivilegiju(mpdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj/admin")]
        public void DodajAdminPrivilegiju([FromBody]AdminPrivilegijaDTO apdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.
                DodajAdminPrivilegiju(apdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj/interfejs")]
        public void DodajInterfejsPrivilegiju([FromBody]InterfejsPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.
                DodajInterfejsPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPut]
        [Route("azuriraj/interfejs")]
        public void AzurirajInterfejsPrivilegiju([FromBody]InterfejsPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.AzurirajInterfejsPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPut]
        [Route("azuriraj/admin")]
        public void AzurirajAdminPrivilegiju([FromBody]AdminPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.AzurirajAdminPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPut]
        [Route("azuriraj/meni")]
        public void AzurirajMeniPrivilegiju([FromBody]MeniPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.AzurirajMeniPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPut]
        [Route("azuriraj/funkcionalna")]
        public void AzurirajFunkcionalnuPrivilegiju([FromBody]FunckionalnaPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.AzurirajFunkcionalnuPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi/funkcionalna")]
        public void ObrisiFunckionalnuPrivilegiju([FromBody]FunckionalnaPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.ObrisiFunckionalnuPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi/admin")]
        public void ObrisiAdminPrivilegiju([FromBody]AdminPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.ObrisiAdminPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi/meni")]
        public void ObrisiMeniPrivilegiju([FromBody]MeniPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.ObrisiMeniPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi/interfejs")]
        public void Obrisi([FromBody]InterfejsPrivilegijaDTO ipdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.ObrisiInterfejsPrivilegiju(ipdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

    }
}
