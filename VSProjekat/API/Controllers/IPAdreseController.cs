﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;

namespace API.Controllers
{
    [RoutePrefix("api/ipadrese")]
    public class IPAdreseController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<IPDTO> SveAdrese()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<IPDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.VratiSveIP();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("ip")]
        public IPDTO VratiIpAdresu([FromUri] string ipkey)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            IPDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderPrivilegija.VratiIP(ipkey);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpPost]
        [Route("kreiraj")]
        public void DodajIpAdresu([FromBody] IPDTO ip)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.DodajIP(ip);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi")]
        public void Obrisi([FromBody] IPDTO ip)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderPrivilegija.IzbrisiIP(ip);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        //Kako je ip adresa istovremeno i primarni ključ i jednia kolona u tabeli, ažuriranje nema smisla
    }
}
