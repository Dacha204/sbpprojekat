﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;

namespace API.Controllers
{
    [RoutePrefix("api/grupe")]
    public class GrupeController : ApiController
    {
        // INTERNA UPOTREBA KORISNICKE GRUPE
        // (KORISNIKA ENKAPSULRIAMO U KORISICNICKU GRUPU -> OPERACIJE SA PRIVILEGIJAMA
        // RADIMO NA NIVOU GRUPE)


        //[HttpGet]
        //[Route("")]
        //public List<GrupaDTO> SveGrupe()
        //{
        //    Privilegije_korisnika.DataLayer.OtvoriSesiju();
        //    List<GrupaDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderGrupa.SveGrupe();
        //    Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        //    return rezultat;
        //}

        //[HttpGet]
        //[Route("korisnicke/vrati/{id:int}")]
        //public KorisnickaGrupaDTO VratiKorisnicikuGrupu(int id)
        //{
        //    Privilegije_korisnika.DataLayer.OtvoriSesiju();
        //    KorisnickaGrupaDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderGrupa.VratiKorisnickuGrupu(id);
        //    Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        //    return rezultat;
        //}

        //[HttpPut]
        //[Route("korisnicke/azuriraj")]
        //public void AzurirajKorisnickuGrupu([FromBody] KorisnickaGrupaDTO k)
        //{
        //    Privilegije_korisnika.DataLayer.OtvoriSesiju();
        //    Privilegije_korisnika.Provajderi.ProvajderGrupa.AzurirajKorisnickuGrupu(k);
        //    Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        //}

        //[HttpDelete]
        //[Route("korisnicke/obrisi")]
        //public void ObrisiKorisnickuGrupu([FromBody] KorisnickaGrupaDTO k)
        //{
        //    Privilegije_korisnika.DataLayer.OtvoriSesiju();
        //    Privilegije_korisnika.Provajderi.ProvajderGrupa.ObrisiKorisnickuGrupu(k);
        //    Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        //}

        // Dodavanje korisnicke grupe se desava prilikom kreiraja korisnika

        [HttpGet]
        [Route("vrati/{id:int}")]
        public RegularnaGrupaDTO VratiRegularnuGrupu(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            RegularnaGrupaDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderGrupa.VratiRegularnuGrupu(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpPut]
        [Route("azuriraj")]
        public void AzurirajRegularnuGrupu([FromBody] RegularnaGrupaDTO k)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderGrupa.AzurirajRegularnuGrupu(k);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi")]
        public void ObrisiRegularnuGrupu([FromBody] RegularnaGrupaDTO k)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderGrupa.ObrisiRegularnuGrupu(k);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj")]
        public void DodajKorisnickuGrupu([FromBody] RegularnaGrupaDTO k)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderGrupa.DodajRegularnuGrupu(k);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }
    }
}
