﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;
using Privilegije_korisnika.Provajderi;


namespace API.Controllers
{
    [RoutePrefix("api/profili")]
    public class ProfiliController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<ProfilDTO> SviProfili()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<ProfilDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderKorisnik.SviProfili();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("vrati/{id:int}")]
        public ProfilDTO VratiProfil(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            ProfilDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderKorisnik.VratiProfil(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpPut]
        [Route("azuriraj")]
        public void AzurirajProfil([FromBody] ProfilDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.PrifilUpdate(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi")]
        public void ObrisiProfil([FromBody] ProfilDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.ObrisiProfil(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPost]
        [Route("dodaj")]
        public void DodajProfil([FromBody] ProfilDTO pdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.DodajProfil(pdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }
    }
}
