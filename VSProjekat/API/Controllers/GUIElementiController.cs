﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Privilegije_korisnika.Provajderi.DTO;

namespace API.Controllers
{
    [RoutePrefix("api/guielementi")]
    public class GUIElementiController : ApiController
    {
        [HttpGet]
        [Route("")]
        public List<GuiElementDTO> SviElementi()
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            List<GuiElementDTO> rezultat = Privilegije_korisnika.Provajderi.ProvajderKorisnik.VratiGUIElemente();
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpGet]
        [Route("vrati/{id:int}")]
        public GuiElementDTO GuiElement(int id)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            GuiElementDTO rezultat = Privilegije_korisnika.Provajderi.ProvajderKorisnik.VratiGUElement(id);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
            return rezultat;
        }

        [HttpPost]
        [Route("kreiraj")]
        public void KreirjElement([FromBody] GuiElementDTO gdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.KreirajGUIElement(gdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpPut]
        [Route("azuriraj")]
        public void AzurirajElement([FromBody] GuiElementDTO gdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.AzurirajGUIElement(gdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }

        [HttpDelete]
        [Route("obrisi")]
        public void ObrisiElement([FromBody] GuiElementDTO gdto)
        {
            Privilegije_korisnika.DataLayer.OtvoriSesiju();
            Privilegije_korisnika.Provajderi.ProvajderKorisnik.ObrisiGUIElement(gdto);
            Privilegije_korisnika.DataLayer.ZatvoriSesiju();
        }
    }
}
