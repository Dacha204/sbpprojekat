﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class RegularnaGrupaMapiranje : SubclassMap<RegularnaGrupa>
    {
        public RegularnaGrupaMapiranje()
        {
            DiscriminatorValue(0);

            HasManyToMany(x => x.SadrziGrupe)
                .Table("PRJ_PRIPADA_GRUPI")
                .ParentKeyColumn("GRoditeljID")
                .ChildKeyColumn("GDeteID");

            HasManyToMany(x => x.PripadaGrupama)
                .Table("PRJ_PRIPADA_GRUPI")
                .ParentKeyColumn("GDeteID")
                .ChildKeyColumn("GRoditeljID")
                .Cascade.SaveUpdate()
                .Inverse();

            HasManyToMany(x => x.SadrziKorisnike)
                .Table("PRJ_PRIPADA_GRUPI")
                .ParentKeyColumn("GRoditeljID")
                .ChildKeyColumn("GDeteID");
        }
    }
}
