﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class SifreMapiranje : ClassMap<Sifre>
    {
        public SifreMapiranje()
        {
            Table("PRJ_SIFRE");

            Id(x => x.SifraId, "SifraID").GeneratedBy.TriggerIdentity();

            Map(x => x.Sifra, "Sifra").Not.Nullable();
            Map(x => x.DatumPostavljanja, "DatumPostavljanja").Not.Nullable();
            Map(x => x.Trenutna, "TrenutnaFlag").Not.Nullable();

            References(x => x.Korisnik, "KorisnikID").Not.Nullable();
        }
    }
}
