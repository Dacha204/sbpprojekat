﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class GuiElementMapiranje : ClassMap<GuiElement>
    {
        public GuiElementMapiranje()
        {
            Table("PRJ_GUI_ELEMENT");

            Id(x => x.ElementId).Column("ElementID").GeneratedBy.TriggerIdentity();

            Map(x => x.Naziv).Column("Naziv");

            HasManyToMany(x => x.Profili)
                .Table("PRJ_PROFIL_PRIKAZUJE")
                .ParentKeyColumn("ElementID")
                .ChildKeyColumn("RedniBroj")
                .Inverse().Cascade.SaveUpdate();
        }
    }
}
