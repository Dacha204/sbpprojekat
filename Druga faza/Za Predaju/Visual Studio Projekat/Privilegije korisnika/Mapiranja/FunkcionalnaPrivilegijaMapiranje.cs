﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class FunkcionalnaPrivilegijaMapiranje : SubclassMap<FunkcionalnaPrivilegija>
    {
        public FunkcionalnaPrivilegijaMapiranje()
        {
            Table("PRJ_FUNKCIONALNA_PRIVILEGIJA");
            KeyColumn("PrivilegijaID");
        }
    }
}
