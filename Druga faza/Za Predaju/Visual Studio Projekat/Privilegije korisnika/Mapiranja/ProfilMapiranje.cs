﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class ProfilMapiranje :ClassMap<Profil>
    {
        public ProfilMapiranje()
        {
            Table("PRJ_PROFIL");

            Id(x => x.RedniBroj, "RedniBroj").GeneratedBy.TriggerIdentity();
            Map(x => x.BojaPozadine, "BojaPozadine");

            References(x => x.Korisnik)
                .Column("KorisnikID")
                .Not.Nullable();

            HasManyToMany(x => x.GuiElementi)
                .Table("PRJ_PROFIL_PRIKAZUJE")
                .ParentKeyColumn("RedniBroj")
                .ChildKeyColumn("ElementID");
        }
    }
}
