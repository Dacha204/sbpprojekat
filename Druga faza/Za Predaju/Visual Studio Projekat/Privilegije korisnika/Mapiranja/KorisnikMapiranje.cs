﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class KorisnikMapiranje : ClassMap<Korisnik>
    {
        public KorisnikMapiranje()
        {
            Table("PRJ_KORISNIK");
            Id(x => x.KorisnikId, "KorisnikID").GeneratedBy.TriggerIdentity();

            Map(x => x.KorisnickoIme, "KorisnickoIme").Not.Nullable().Unique();
            Map(x => x.Ime, "Ime").Not.Nullable();
            Map(x => x.ImeRoditelja, "ImeRoditelja").Not.Nullable();
            Map(x => x.Prezime, "Prezime").Not.Nullable();
            Map(x => x.Jmbg, "JMBG").Not.Nullable();
            Map(x => x.DatumRodjenja, "DatumRodjenja").Not.Nullable();
            Map(x => x.RadnoMesto, "RadnoMesto");
            Map(x => x.Funkcija, "Funkcija");
            Map(x => x.BrojKancelarije, "BrojKancelarije");
            Map(x => x.Telefon, "Telefon");
            Map(x => x.Email, "Email").Not.Nullable();

            References(x => x.KorisnickaGrupa, "KGrupaID").Not.Nullable().Cascade.All();

            HasMany(x => x.Sifre)
                .KeyColumn("KorisnikID")
                .Cascade.AllDeleteOrphan()
                .Inverse();
            
            HasMany(x => x.Prijave)
                .KeyColumn("KorisnikID")
                .Cascade.All()
                .Inverse();

            HasMany(x => x.Profili)
                .KeyColumn("KorisnikID")
                .Cascade.All()
                .Inverse();

            HasMany(x => x.DodelioPrivilegije)
                .KeyColumn("DodelioKorisnikID")
                .Cascade.All()
                .Inverse();
        }
    }
}
