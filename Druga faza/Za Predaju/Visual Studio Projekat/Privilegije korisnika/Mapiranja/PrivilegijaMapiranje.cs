﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class PrivilegijaMapiranje : ClassMap<Privilegija>
    {
        public PrivilegijaMapiranje()
        {
            Table("PRJ_PRIVILEGIJA");

            Id(x => x.PrivilegijaId, "PrivilegijaID").GeneratedBy.TriggerIdentity();

            Map(x => x.Naziv, "Naziv").Not.Nullable();

            HasMany(x => x.ImaDodeleGrupama)
                .Table("PRJ_DODELA_PRIVILEGIJA")
                .KeyColumn("PrivilegijaID")
                .Cascade.SaveUpdate()
                .Inverse();    
        }

    }
}
