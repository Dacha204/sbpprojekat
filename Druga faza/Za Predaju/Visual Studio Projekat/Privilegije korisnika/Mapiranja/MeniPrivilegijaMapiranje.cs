﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class MeniPrivilegijaMapiranje : SubclassMap<MeniPrivilegija>
    {
        public MeniPrivilegijaMapiranje()
        {
            Table("PRJ_MENI_PRIVILEGIJA");
            KeyColumn("PrivilegijaID");

            References(x => x.RoditeljskiMeni, "RoditeljskiMeni");

            HasMany(x => x.Podkontrole)
                .KeyColumn("RoditeljskiMeni")
                .Cascade.SaveUpdate()
                .Inverse();
        }
    }
}
