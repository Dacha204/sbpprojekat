﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class DodelaPrivilegijaMapiranje : ClassMap<DodelaPrivilegijeVeza>
    {
        public DodelaPrivilegijaMapiranje()
        {
            Table("PRJ_DODELA_PRIVILEGIJA");

            CompositeId(x => x.DodelaID)
                .KeyReference(x => x.PrivilegijaID)
                .KeyReference(x => x.GrupaID);

            Map(x => x.DatumDodele, "DatumDodele").Not.Nullable();
            References(x => x.DodelioKorisnik, "DodelioKorisnikID").Not.Nullable();
        }
    }
}
