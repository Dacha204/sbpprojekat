﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class IpMapiranje : ClassMap<Ip>
    {
        public IpMapiranje()
        {
            Table("PRJ_IP");

            Id(x => x.IpAdresa).Column("IPAdresa").GeneratedBy.TriggerIdentity();

            HasManyToMany(x => x.Grupe)
                .Table("PRJ_DOZVOLJENE_IP")
                .ParentKeyColumn("IpAdresa")
                .ChildKeyColumn("GrupaID")
                .Inverse().Cascade.SaveUpdate();
        }
    }
}
