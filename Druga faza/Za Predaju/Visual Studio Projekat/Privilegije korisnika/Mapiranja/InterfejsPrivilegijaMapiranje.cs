﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Helpers;
using FluentNHibernate.Mapping;
using Privilegije_korisnika.Entiteti;

namespace Privilegije_korisnika.Mapiranja
{
    public class InterfejsPrivilegijaMapiranje : SubclassMap<InterfejsPrivilegija>
    {
        public InterfejsPrivilegijaMapiranje()
        {
            Table("PRJ_INTERFEJS_PRIVILEGIJA");
            Map(x => x.DozvolaKoriscenja).Column("DozvolaKoriscenja");
           
            KeyColumn("PrivilegijaID");
        }
    }
}
