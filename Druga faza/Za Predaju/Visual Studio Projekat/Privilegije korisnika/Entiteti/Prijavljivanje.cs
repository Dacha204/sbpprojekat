﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.ComponentModel.Com2Interop;

namespace Privilegije_korisnika.Entiteti
{
    public class Prijavljivanje
    {
        public virtual int PrijavaId { get; set; }
        public virtual bool Uspesna { get; set; }
        public virtual string IpAdresa { get; set; }
        public virtual DateTime Vreme { get; set; } 
        public virtual string Sifra { get; set; }

        public virtual Korisnik Korisnik { get; set; }
    }
}
