﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class MeniPrivilegija : Privilegija
    {
        public virtual MeniPrivilegija RoditeljskiMeni { get; set; }

        public virtual IList<MeniPrivilegija> Podkontrole { get; set; }

        public MeniPrivilegija()
        {
            Podkontrole = new List<MeniPrivilegija>();
        }
    }
}
