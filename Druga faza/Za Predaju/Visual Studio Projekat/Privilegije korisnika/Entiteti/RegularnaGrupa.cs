﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class RegularnaGrupa : Grupa
    {
        
        public virtual IList<RegularnaGrupa> SadrziGrupe { get; set; }
        public virtual IList<RegularnaGrupa> PripadaGrupama { get; set; }
        public virtual IList<KorisnickaGrupa> SadrziKorisnike { get; set; }

        public RegularnaGrupa()
        {
            SadrziGrupe = new List<RegularnaGrupa>();
            PripadaGrupama = new List<RegularnaGrupa>();
            SadrziKorisnike = new List<KorisnickaGrupa>();
        }
    }
}
