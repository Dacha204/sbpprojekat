﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Privilegije_korisnika.Entiteti
{
    public class KorisnickaGrupa : Grupa
    {
        public virtual Korisnik Korisnik { get; set; }
        public virtual IList<RegularnaGrupa> PripadaGrupama { get; set; }
        
        public KorisnickaGrupa()
        {
            PripadaGrupama = new List<RegularnaGrupa>();
        }
    }
}
